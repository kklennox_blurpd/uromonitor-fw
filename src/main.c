/* ========================================
 *
 * Copyright Blur Product Development, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur Product Development.
 *
 * ========================================
*/
/**
 * @brief BrightUro Application main file.
 *
 * This file contains the source code for the BrightUro firmware application.
 */

#include <stdint.h>
#include <string.h>

#include "project.h"
#include "globals.h"

#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "boards.h"
#include "app_timer.h"
#include "app_button.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_drv_rtc.h"
#include "nrf_drv_clock.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "BlurCLI.h"

#include "gpio_handler.h"
#include "i2c_handler.h"
#include "ble_handler.h"
#include "UARTHandler.h"
#include "CommandHandler.h"
#include "SPIHandler.h"
#include "FlashInterface.h"
#include "PacketHandler.h"
#include "RTCInterface.h"

#include "pressure_LPS22HB.h"

#include "nrf_drv_gpiote.h"
 
#define DIGOUT_PWRON_LATCH              14                        /**< GPIO Pin that will latch power on, or allow device to shut itself off. */          
#define DIGOUT_MEM_PWR_B                5                    
#define DIGOUT_LED_2                    28         

#define DEAD_BEEF                       0xDEADBEEF                /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

sShellImpl shell_impl;
volatile uint8_t ucGlobalFlags1_;
uint32_t uiNumStoredTimesteps = 0;
Timestep_t _sTimestep;
Timestep_t _sTimestepReadback;
uint32_t uiPressureBuf[FIFO_SIZE];
uint8_t ucPressureLoop;
uint32_t uiFakeTimeStamp = 0;
uint16_t uiSeqNum = 0;
bool bDataCheck;
uint32_t uiTestTimestamp;
uint32_t uiTimeRunning = 0;

// CLI
// wrapper for RTT terminal 0 for CLI
int send_char_wrapper(char c)
{
  return(SEGGER_RTT_PutChar(CLI_RTT_TERMINAL_NUM,c));
}

/**@brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module.
 */
static void timers_init(void)
{
    // Initialize timer module, making it use the scheduler
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}


static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */
static void idle_state_handle(void)
{
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}

static void lfclk_config(void)
{
    ret_code_t err_code = nrf_drv_clock_init();
    APP_ERROR_CHECK(err_code);

    nrf_drv_clock_lfclk_request(NULL);
}

/**@brief Function for application main entry.
 */

Timestep_t timestep;
uint32_t num_timesteps;
Timestep_t testStep;

int main(void)
{
    // See this discussion on using RC oscillator instead of external crystal: 
    // https://devzone.nordicsemi.com/f/nordic-q-a/40160/custom-nrf52832-board-only-boots-right-after-flashing
    log_init();
    timers_init();
    gpio_handler_init();
    power_management_init();

    UARTHandlerInit();
    commandHandlerInit();
    i2c_init();
    SPIHandlerInit();
    
    //ble_stack_init();
    //gap_params_init();
    //gatt_init();
    //services_init();
    //advertising_init();
    //conn_params_init();

    // Start CLI shell
    shell_boot(&shell_impl);

    // Start execution.
    NRF_LOG_INFO("Application started.");
    //advertising_start();

    nrf_gpio_cfg_output(SPI_CS_PIN);

    nrf_gpio_cfg_output(UART_ENABLE_PIN);

    nrf_gpio_pin_write(DIGOUT_PWRON_LATCH, 1);

    nrf_gpio_cfg_output(DIGOUT_LED_2);
    nrf_gpio_pin_write(DIGOUT_LED_2, 0);
    
    LPS22HB_init();
    //PacketHandlerInit();
    lfclk_config();
    RTCInterfaceInit();

    // Make sure flash is communicating
    while(!FlashIsPresent()) {}
    
    //Erase flash on boot
    flashEraseTimestepBlock();

    // Get number of timesteps present
    num_timesteps = flashScanForLastTimestep();
    uiFakeTimeStamp = 3 * num_timesteps;
    uiSeqNum = num_timesteps;
    
    for (;;)
    {
      //idle_state_handle();
      
      if(CHECK_BIT(ucGlobalFlags1_, eGlobalFlag1_TimerTick))
      {
          uiTimeRunning++;

          NRF_LOG_INFO("Running for %is", uiTimeRunning);
          NRF_LOG_FLUSH();

          RTCInterfaceClearCompare0Int();

          RESET_BIT(ucGlobalFlags1_, eGlobalFlag1_TimerTick);
      }
      
      //Check if FIFO Flag is Set
      if(CHECK_BIT(ucGlobalFlags1_, eGlobalFlag1_PressureFifoFull))
      {     
          //Handle Read
          LSP22HB_read_fifo();

          //Clear Fifo
          LSP22HB_clear_fifo();

          //Set Data Ready Flag
          BLUR_SET_BIT(ucGlobalFlags1_, eGlobalFlag1_DataReady);
          
          //Reset Fifo Full Bit
          RESET_BIT(ucGlobalFlags1_, eGlobalFlag1_PressureFifoFull);
      }
      
      //Check if Data Ready Flag is set
      if(CHECK_BIT(ucGlobalFlags1_, eGlobalFlag1_DataReady))
      {     
          //Slap together a packet
          _sTimestep.uiTimeStamp = uiFakeTimeStamp; //Placeholder - RTC in Progress
          uiFakeTimeStamp += 3;

          _sTimestep.uiSequenceNum = uiSeqNum;
          uiSeqNum += 1;

          _sTimestep.ucTimeStepFlags = eTimeStepFlag_Sensor_Reading;

          LPS22HB_GetFifo(uiPressureBuf);
          for(ucPressureLoop = 0; ucPressureLoop < FIFO_SIZE; ucPressureLoop++)
          {
              _sTimestep.uiPressure[ucPressureLoop] = uiPressureBuf[ucPressureLoop];
          }

          _sTimestep.iVbatt = 100;
          _sTimestep.ucChecksum = PacketHandlerCalculateChecksum(&_sTimestep);
            
          
          //Slap packet into Flash
          flashWriteTimestepData(num_timesteps, &_sTimestep);

          //Read back timestamp
          flashReadTimestepData(num_timesteps, &_sTimestepReadback);

          //Check to make sure written step = read step
          bDataCheck = flashCompareTimestepData(&_sTimestep, &_sTimestepReadback);

          //Try again
          if(!bDataCheck)
          {
            //Write again
            flashWriteTimestepData(num_timesteps, &_sTimestep);
            //Read again
            flashReadTimestepData(num_timesteps, &_sTimestepReadback);
            //Check again
            bDataCheck = flashCompareTimestepData(&_sTimestep, &_sTimestepReadback);

            if(!bDataCheck)
            {
                //Go to Error Handler - while(1) with Error Indication via either UART, BLE, or LED
            }
          }
          else
          {
              //Increment num_timesteps
              num_timesteps++;
              flashUpdateNumSteps(num_timesteps);

              //Log msg that Timestep was stored
              NRF_LOG_INFO("Timestep %i stored",num_timesteps);
              NRF_LOG_FLUSH();
          }
          
          //Reset Data Ready Bit
          RESET_BIT(ucGlobalFlags1_, eGlobalFlag1_DataReady);
      }
        
      //Read any data present in the input serial FIFO
      UARTHandlerProcessEvents();
      if(true == commandHandlerHasCompleteIncomingCommand())
      {
          //Process incoming command
          commandHandlerProcessIncomingCommand();
          //write buffer
          //nrf_delay_ms(5);
          UARTHandlerWriteBuffer(commandHandlerGetResponseByteCount());
          //reset uart
          UARTHanlderReset();
      }
    
      //Check if Chip Erased Flag is set
      if(CHECK_BIT(ucGlobalFlags1_, eGlobalFlag1_ChipErased))
      { 
          //Reset variables associated with timestamp
          uiFakeTimeStamp = 0;
          num_timesteps = 0;
          uiSeqNum = 0;
          
          //Reset Chip Erased Bit
          RESET_BIT(ucGlobalFlags1_, eGlobalFlag1_ChipErased); 
      } 

      idle_state_handle();

      // Check if character is received over RTT, if so, pass to shell
      if(SEGGER_RTT_HasKey())
      {
        shell_receive_char(SEGGER_RTT_GetKey());
      }
    }
}


/**
 * @}
 */
