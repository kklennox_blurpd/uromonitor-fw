##############
System Testing
##############

System testing is the integrative testing of the entire device, as it will be deployed in the field. 
This type of testing verifies that the device behaves as it should, and works to replicate outside stimuli
on the device as accurately as possible to what it will see in the field.

These test steps, unlike `unit testing <_setup-unit-testing>`__, do not easily mock via a test framework. 
Generally, these steps must toggle the devices inputs by hand, or with outside hardware interfacing tools (Raspberry Pi, etc).

.. contents::

==========================
Device Pairing/Bonding 
==========================

==========================
Device Information Service 
==========================

======================
Service Changed
======================

Test Number : ?
Test Name : Device should expose service changed characteristic
Device Output method : BLE
Device Function Under Test : Generic Attribute Service Changed
Givens : No sensor is connected, dock is connected
Test Input : Connect to device over BLE
Expected Output : Service Changed characteristic should be exposed 

===================================================
Battery Service, Battery Level Notifications
===================================================

Test Number : ?
Test Name : Device should handle notification when battery level updated
Device Output method : BLE
Device Function Under Test : Battery level characteristic
Givens : No sensor is connected, no dock is connected
Test Input : Connect dock
Expected Output : Battery level percent should slowly increase over 10 minutes

===================================================
Device Service, Status Characteristic Notifications
===================================================

Test Number : ?
Test name : Device should handle incrementing timestep count from log file
Device Output method : BLE
Device Function Under Test : sDevice characteristic -> Status
Givens : Sensor is disconnected, dock is disconnected
Test Input : Connect a sensor
Expected Output : sDevice->timestep_count should increment by 1

Test Number : ?
Test name : Device should handle incrementing timestep count from sampled sensor
Device Output method : BLE
Device Function Under Test : sDevice characteristic -> Status
Givens : Sensor is disconnected, dock is disconnected
Test Input : Connect a sensor, wait until at least 1 sample has been taken
Expected Output : sDevice->timestep_count should increment by 2, once from sensor_connect event and one from sample_sensor with sensor Data

Test Number : ?
Test name : Device should handle notification of new stream_data from sampled data
Device Output method : BLE
Device Function Under Test : sDevice characteristic -> Status
Givens : Sensor is connected, dock is disconnected
Test Input : None
Expected Output : sDevice->stream_data updating every sample rate

Test Number : ?
Test name : Device should handle notification of new stream_data from log file addition
Device Output method : BLE
Device Function Under Test : sDevice characteristic -> Status
Givens : Sensor is connected, dock is disconnected
Test Input : Disconnect sensor
Expected Output : sDevice->stream_data updating to show sensor_disconnected log entry

=================
Flash Log Storage 
=================

Test Number : ?
Test name : Device should correctly log Startup 
Device Output method : Flash
Device Function Under Test : Log -> DockDisconnected
Givens : Sensor is disconnected, dock is connected
Test Input : Force system reset via CLI
Expected Output : Retrieval of flash entries should show Startup entry

Test Number : ?
Test name : Device should correctly log Low Battery
Device Output method : Flash
Device Function Under Test : Log -> LowBattery
Givens : Sensor is Connected, dock is Disconnected
Test Input : Allow system voltage to drop below low battery threshold
Expected Output : Retrieval of flash entries should show LowBattery entry

Test Number : ?
Test name : Device should correctly log RTC set 
Device Output method : Flash
Device Function Under Test : Log -> RTC Set
Givens : Sensor is disconnected, dock is connected
Test Input : Set RTC via BLE, log previous time before set via CLI or BLE
Expected Output : Retrieval of flash entries should show RTC Set entry
Expected Output : Data in RTC set log should show previous time, to calculate delta T
Expected Output : Retrieval of RTC time via BLE should show correct time

Test Number : ?
Test name : Device should correctly log Flash Erase
Device Output method : Flash
Device Function Under Test : Log -> LowBattery
Givens : Sensor is disconnected, dock is disconnected
Test Input : Issue flash erase BLE command
Expected Output : Retrieval of flash entries should show Flash Erase entry

Test Number : ?
Test name : Device should correctly NOT log Factory Reset CLI command
Device Output method : Flash
Device Function Under Test : Log -> LowBattery
Givens : Sensor is disconnected, dock is disconnected
Test Input : Issue factory reset CLI command
Expected Output : Retrieval of flash entries should show zero

======================
Flash Timestep Storage 
======================

=================
UART CLI Commands
=================

==============
BLE OpCommands 
==============

=================
Bulk Download 
=================

=================
LED Indication 
=================