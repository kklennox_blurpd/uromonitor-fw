#######################
Flashing via Programmer
#######################

Version V7.62b was used to generate these instructions. A programming jig is required.

.. warning:: 
	A special programming adapter is required. The programming port pinout on the device is modified.

********************
JFlash-Lite Settings
********************

1) Insert device into programming jig, and provide power to the device.
2) Select the hex file to be programmed.
3) Program the device as normal.