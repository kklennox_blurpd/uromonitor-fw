/* ========================================
 *
 * Copyright BlurPD, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur PD 2022
 *
 * ========================================
*/

#ifndef RTCINTERFACE_H
#define RTCINTERFACE_H

#include "globals.h"
#include "nrf_drv_rtc.h"
#include "nrf_drv_clock.h"

void RTCInterfaceInit();

void RTCInterfaceSetTime(uint32_t time);
uint32_t RTCInterfaceGetTime();

uint32_t RTCInterfaceGetCounts();
void RTCInterfaceClearCompare0Int();

#endif
/* [] END OF FILE */
