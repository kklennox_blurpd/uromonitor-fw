/* ========================================
 *
 * Copyright Blur PD 2020
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF BLUR PD.
 *
 * ========================================
*/

#ifndef BLUR_PACKET_PROTOCOL_H
#define BLUR_PACKET_PROTOCOL_H
    
typedef enum eBPPPacketParseErrors
{
    eBPPPacketParseError_None = 0,
    eBPPPacketParseError_Invalid_Start_Delimiter,
    eBPPPacketParseError_Invalid_Min_Packet_Size,
    eBPPPacketParseError_Invalid_Commmand_ID,
    eBPPPacketParseError_Invalid_Packet_Length_In_Packet,
    eBPPPacketParseError_Packet_Length_Mismatch,
    eBPPPacketParseError_Invalid_Source_ID,
    eBPPPacketParseError_Invalid_Destination_ID,
    eBPPPacketParseError_Invalid_Sequence_Number,
    eBPPPacketParseError_Invalid_Data_Type,
    eBPPPacketParseError_Invalid_Data_End_Delimiter,
    eBPPPacketParseError_Invalid_Checksum,
    eBPPPacketParseError_Checksum_Mismatch,
    eBPPPacketParseError_Invalid_Terminator1_Delimiter,
    eBPPPacketParseError_Invalid_Terminator2_Delimiter
} eBPPPacketParseErrors;

#define BLURPP_DATATYPE_ASCII   0x41u   //'A'
#define BLURPP_DATATYPE_BINARY  0x42u   //'B'
#define BLURPP_DATATYPE_NONE    0x30u   //'0' zero
    
//This must be called before initiating any other functions to set the pointers for the operating buffers
extern void blurPPInitializeBuffers(unsigned char *pucaTransmitBuffer, unsigned int uiTransmitBufferSize, unsigned char *pucaReceiveBuffer, unsigned int uiReceiveBufferSize);
//Return non-zero if receive buffer contains the terminator character sequence, otherwise returns 0
extern unsigned char blurPPTerminatorInReceiveBuffer(unsigned int uiInputCount);
//Returns a member of eBPPPacketParseErrors to determine if receive buffer contains a valid incoming packet
extern eBPPPacketParseErrors blurPPValidateIncomingPacket(unsigned int uiInputCount);

//Accessors for incoming data packet
//Returns the command ID of the incoming command packet (valid after blurPPValidateIncomingPacket)
extern unsigned char blurPPIncomingCommandID();
//Returns the source ID for the incoming command packet (valid after blurPPValidateIncomingPacket)
extern unsigned char blurPPIncomingSourceID();
//Returns the destination ID for the incoming command packet (valid after blurPPValidateIncomingPacket)
extern unsigned char blurPPIncomingDestinationID();
//Returns the sequence number for the incoming command packet (valid after blurPPValidateIncomingPacket)
extern unsigned short int blurPPIncomingSequenceNumber();
//Returns the data type for the incoming command packet (valid after blurPPValidateIncomingPacket)
extern unsigned char blurPPIncomingDataType();
//Returns the number of data bytes transmitted in the incoming data packet (valid after blurPPValidateIncomingPacket)
extern unsigned int blurPPIncomingDataByteCount();
//Returns the debug data for validation errors
extern unsigned char blurPPValidationFailureDebugData();
//Returns the next byte in the data payload (valid after blurPPValidateIncomingPacket)
extern unsigned char blurPPInputDataByte();
//Returns the next ushort in the data payload (valid after blurPPValidateIncomingPacket)
extern unsigned short int blurPPInputDataShort();
//Returns the next uint in the data payload (valid after blurPPValidateIncomingPacket)
extern unsigned int blurPPInputDataInt();
//Returns the next float in the data payload (valid after blurPPValidateIncomingPacket)
extern float blurPPInputDataFloat();
//Returns the next double in the data payload (valid after blurPPValidateIncomingPacket)
extern double blurPPInputDataDouble();
//Extracts an data as an array of bytes (valid after blurPPValidateIncomingPacket)
extern void blurPPInputDataByteArray(unsigned char*,unsigned int uiCount);

//Accessors for outgoing packets
//Set the source ID for the outgoing packet (optional)
extern void blurPPSetOutgoingSourceID(unsigned char ucID);
//Set the destination ID for the outgoing packet (optional)
extern void blurPPSetOutgoingDestinationID(unsigned char ucID);
//Set the sequence number for the outgoing packet
extern void blurPPSetOutgoingSequenceNumber(unsigned short int uiSeq);
//Start a new packet for transmission
extern void blurPPBeginNewPacket(unsigned char ucCommandID, unsigned char ucDataType);
//Set a data byte into the outgoing packet
extern void blurPPOutputDataByte(unsigned char ucData);
//Set a short int into the outgoing packet
extern void blurPPOutputDataShort(unsigned short int uiData);
//Set a int into the outgoing packet
extern void blurPPOutputDataInt(unsigned int uiData);
//Set a float into the outgoing packet
extern void blurPPOutputDataFloat(float fData);
//Set a double into the outgoing packet
extern void blurPPOutputDataDouble(double dData);
//Set an array of bytes as teh data
extern void blurPPOutputDataByteArray(unsigned char*,unsigned int uiCount);
//Terminate the outgoing packet to make it ready to send
extern void blurPPTerminatePacket();
//Return the total packet size of the outgoing packet (valid after blurPPTerminatePacket)
extern unsigned int blurPPOutgoingPacketSize();
//Setup new error response packet
extern void blurPPNewErrorPacket(unsigned char cErrorData);
    
#endif

/* [] END OF FILE */
