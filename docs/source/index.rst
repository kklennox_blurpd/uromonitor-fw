Welcome to Uromonitor firmware documentation!
=============================================

.. toctree::
	:maxdepth: 2
	:caption: Table of Contents:

	setup/setup-overview
	usage/usage-overview
	testing/testing-overview
	architecture/architecture-overview
	

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
