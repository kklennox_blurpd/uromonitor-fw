###########
Data Format
###########

.. |packet-size| replace:: 130

The Uromonitor flash memory section allocated to system data is organized into fixed-size packets of |packet-size| bytes. 
These packets each have an identifier which specify the type of data they contain:

.. code-block:: c

  enum eTimeStepFlags
  {
      eTimeStepFlag_Sensor_Reading    =       0x01, 
      eTimeStepFlag_Log               =       0x02
  };

All timesteps contain the same base fields, with only small differences between them. 
There are currently 2 different types of ``Timestep_t`` (|packet-size| bytes), dictated by ``ucTimestepFlags``. 
All types contain the same data formats and order/packing. 
All types share ``uiTimestamp``, incrementing ``uiSequenceNum``, and battery voltage ``iVbatt``. 
Types differ in ``ucTimestepFlags``, and data contained in ``uiPressure``.

.. _packet-format:

.. code-block:: c

  #pragma pack(1) //byte aligned
  typedef struct 
  {   
      uint32_t uiTimeStamp;
      uint16_t uiSequenceNum;
      uint8_t ucTimeStepFlags;
      uint32_t uiPressure[30];
      int16_t iVbatt;
      uint8_t ucChecksum;
  } Timestep_t, *p_Timestep_t;
  #pragma pack(0)

The above typedef specifies the Sensor Reading timestep packet. The two differences between the different types of packets are:

1) ucTimestepFlags - will be one of eTimestepFlags above
2) uiPressure[30] - will be a byte array for Log, and an array of int16 for Sensor Reading

Log Timestep
~~~~~~~~~~~~

Log timesteps contain general logging information, such as user interactions. 
Log entries use the first two bytes of uiPressure to denote the log type:

.. code-block:: c

  typedef enum 
  {
      eLogFlag_LowBattery =           0x1010,
      eLogFlag_Startup =              0xAAAA,
      eLogFlag_RtcSet =               0xBBBB,
      eLogFlag_FlashErase =           0xCCCC,
      eLogFlag_PowerDown =            0xDDDD,
  }eLogFlag;

.. note::
  
  Some of the log entries will also have additional data present, such as the RTC Set log entry, which will include the previously set time in the data field.

Sensor Reading Timestep
~~~~~~~~~~~~~~~~~~~~~~~

The sensor reading timestep will include 30 sensor reading values, corresponding to a 10Hz sample rate with 3 seconds of data per Timestep. 