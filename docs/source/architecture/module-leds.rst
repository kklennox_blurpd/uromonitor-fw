LEDS
####

.. _module-leds:

The Uromonitor contains one discrete LED:

.. code-block::

    DEVICE STATUS: R/G capable

Device Status
~~~~~~~~~~~~~

.. list-table:: 
    :widths: 25 50
    :header-rows: 1

    * - Indicator
      - Behavior
    * - Solid Green
      - 
    * - Blinking Green
      - 
    * - Solid Yellow
      - 
    * - Blinking Yellow
      - 
    * - Solid Red
      - 
    * - Blinking Red
      - 

