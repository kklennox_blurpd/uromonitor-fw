#########################
Op Command Characteristic
#########################

.. contents::

.. _uromonitor-characteristic-opcommand:

The Op Command characteristic provides the central device with 
commands to perform several workflows on the SmartLink device.

.. list-table:: 
   :widths: 50 25 25
   :header-rows: 1

   * - UUID
     - Properties
     - Permissions
   * - ``88ED0003-722D-0055-02DF-A2340946D9C0``
     - Read, Write, Notify
     - Encrypted

***********
Data Format
***********

Each Op Command and any responses are of fixed length. The entire packet contains 11 bytes as follows:

.. list-table:: 
   :widths: 25 25 25 50
   :header-rows: 1

   * - Byte Position
     - Format
     - Parameter
     - Description
   * - 1
     - uint8
     - Op Code
     - Command, see Op Codes
   * - 2
     - uint8[8]
     - Operand
     - Data used by the command
   * - 10
     - uint16
     - Checksum
     - 16 bit checksum (Little Endian)

.. note::
    
    Not all commands use all of the available Operand space allocated. 
    Some commands may not use any Operand data at all.

********
Op Codes
********

Op Code 0x01 Identify Device via LED response
=============================================

.. list-table::
   :widths: 25 25 25 50
   :header-rows: 1

   * - Op Code
     - Operands
     - Operand Data Type
     - Description
   * - 0x01
     - N/A
     - N/A
     - Device will display LED to allow for identification

Op Code 0x20 Set RTC
====================

Op Code 0x21 Get RTC
====================

Op Code 0x40 Set User Flags
===========================

.. list-table::
   :widths: 25 25 25 50
   :header-rows: 1

   * - Op Code
     - Operands
     - Operand Data Type
     - Description
   * - 0x40
     - 0x01
     - ``uint8``
     - 

Op Code 0x51 Erase Flash
========================

.. list-table::
   :widths: 25 25 25 50
   :header-rows: 1

   * - Op Code
     - Operands
     - Operand Data Type
     - Description
   * - 0x51
     - N/A
     - 
     - Erase all timestep data in flash memory

Op Code 0x52 Device Shutdown
============================

.. list-table::
   :widths: 25 25 25 50
   :header-rows: 1

   * - Op Code
     - Operands
     - Operand Data Type
     - Description
   * - 0x52
     - N/A
     - 
     - Place the device in low power ship-mode. The device will remain in this mode until the button is pressed.

.. _uromonitor-characteristic-opcommand-bulk-data:

Op Code 0x60 Bulk data download
===============================

.. list-table::
   :widths: 25 25 25 50
   :header-rows: 1

   * - Op Code
     - Operands
     - Operand Data Type
     - Description
   * - 0x60
     - [0,1]
     - ``uint8``
     - Download type, 0 = full download, 1 = single bulk chunk download
   * - 
     - Start index
     - ``uint32``
     - Index of timestep object to download in little endian format

Op Code 0x70 Low latency connection
===================================

.. list-table::
   :widths: 25 25 25 50
   :header-rows: 1

   * - Op Code
     - Operands
     - Operand Data Type
     - Description
   * - 0x70
     - N/A
     - 
     - Request the BLE connection to be placed into a low latency mode for improved responsiveness

Op Code 0x90 Response
=====================

.. list-table::
   :widths: 25 25 25 50
   :header-rows: 1

   * - Op Code
     - Operands
     - Operand Data Type
     - Description
   * - 0x90
     - 0x00
     - ``uint8``
     - Success
   * - 0x90
     - 0x01
     - ``uint8``
     - CRC Error
   * - 0x90
     - 0x02
     - ``uint8``
     - Format Error
   * - 0x90
     - 0x03
     - ``uint8``
     - Unknown Op Code Error
   * - 0x90
     - 0x04
     - ``uint8``
     - Other Error

.. note::
  This is a notify-only Op Code. Writes to this opcode are ignored by the device.