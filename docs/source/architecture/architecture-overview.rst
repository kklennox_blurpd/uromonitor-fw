Architecture
************

.. toctree::
    :maxdepth: 2

    architecture-introduction
    architecture-electrical-schematic
    architecture-modules-overview
    architecture-data
    architecture-ble
    architecture-error-handling
    architecture-special-considerations
    architecture-soup