/* ========================================
 *
 * Copyright Blur Product Development, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur Product Development.
 *
 * ========================================
*/

/**
 * @brief Handles pressure sensor initialization and reading. 
 *
 */
#include "pressure_LPS22HB.h"
#include "project.h"
#include "i2c_handler.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "sdk_config.h"

#include "app_util_platform.h"
#include "app_error.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"

#include "nrf_drv_gpiote.h"
#include "nrfx_gpiote.h"

// Interrupt pin config
#define HI_ACCURACY_DISABLED                              (false)
static const nrfx_gpiote_in_config_t m_gpiote_input_cfg   = {                                                   \
                                                                .sense = NRF_GPIOTE_POLARITY_LOTOHI,            \
                                                                .pull = NRF_GPIO_PIN_NOPULL,                    \
                                                                .is_watcher = false,                            \
                                                                .hi_accuracy = HI_ACCURACY_DISABLED,            \
                                                                .skip_gpio_setup = false,                       \
                                                            };

// Forward declare pin interrupt handler
void sensor_int_evt_handler(nrfx_gpiote_pin_t pin, nrf_gpiote_polarity_t action);


// Pressure sensor datasheet:
// https://www.st.com/content/ccc/resource/technical/document/datasheet/bf/c1/4f/23/61/17/44/8a/DM00140895.pdf/files/DM00140895.pdf/jcr:content/translations/en.DM00140895.pdf

// Pressure sensor i2c address:
// 8 bit = 0xB8 (b1011 100x)
// 7 bit = 0x5C (b0101 1100)

// 7-bit I2C address
#define LPS22HB_I2C_ADDR          (0x5C)         

#define REG_VAL(val, start_bit) (val << start_bit)

#define INTERRUPT_CFG           (0x0B)
  #define AUTORIFP                REG_VAL(0x01, 7)
  #define RESET_ARP               REG_VAL(0x01, 6)
  #define AUTOZERO                REG_VAL(0x01, 5)
  #define RESET_AZ                REG_VAL(0x01, 4)
  #define DIFF_EN                 REG_VAL(0x01, 3)
  #define LIR                     REG_VAL(0x01, 2)
  #define PLE                     REG_VAL(0x01, 1)
  #define PHE                     REG_VAL(0x01, 0)
#define THS_P_L                 (0x0C)
#define THS_P_H                 (0x0D)
#define WHO_AM_I                (0x0F)
  #define CHIP_ID                 REG_VAL(0xB1, 0)
#define CTRL_REG1               (0x10)
  #define POWER_DOWN              REG_VAL(0x00, 4)
  #define ODR_1HZ                 REG_VAL(0x01, 4)
  #define ODR_10HZ                REG_VAL(0x02, 4)
  #define ODR_25HZ                REG_VAL(0x03, 4)
  #define ODR_50HZ                REG_VAL(0x04, 4)
  #define ODR_75HZ                REG_VAL(0x05, 4)
  #define EN_LPFP                 REG_VAL(0x01, 3)
  #define LPFP_CFG                REG_VAL(0x01, 2)
  #define BDU                     REG_VAL(0x01, 1)
  #define SIM                     REG_VAL(0x01, 0)
#define CTRL_REG2               (0x11)
  #define BOOT                    REG_VAL(0x01, 7)
  #define FIFO_EN                 REG_VAL(0x01, 6)
  #define STOP_ON_FTH             REG_VAL(0x01, 5)
  #define IF_ADD_INC              REG_VAL(0x01, 4)
  #define I2C_DIS                 REG_VAL(0x01, 3)
  #define SWRESET                 REG_VAL(0x01, 2)
  #define ONE_SHOT                REG_VAL(0x01, 0)
#define CTRL_REG3               (0x12)
  #define INT_H_L                 REG_VAL(0x01, 7)
  #define PP_OD                   REG_VAL(0x01, 6)
  #define F_FSS5                  REG_VAL(0x01, 5)
  #define F_FTH                   REG_VAL(0x01, 4)
  #define F_OVR                   REG_VAL(0x01, 3)
  #define DRDY                    REG_VAL(0x01, 2)
  #define INT_S                   REG_VAL(0x01, 0)
#define FIFO_CTRL               (0x14)
  #define FIFO_BYP                REG_VAL(0x00, 5)
  #define FIFO_FIFO               REG_VAL(0x01, 5)
  #define FIFO_STREAM             REG_VAL(0x02, 5)
  #define FIFO_STREAM_TO_FIFO     REG_VAL(0x03, 5)
  #define FIFO_BYP_TO_STREAM      REG_VAL(0x04, 5)
  #define FIFO_DYN_STREAM         REG_VAL(0x06, 5)
  #define FIFO_BYP_TO_FIFO        REG_VAL(0x07, 5)
#define REF_P_XL                (0x15)
#define REF_P_L                 (0x16)
#define REF_P_H                 (0x17)
#define RPDS_L                  (0x18)
#define RPDS_H                  (0x19)
#define RES_CONF                (0x1A)
  #define LC_EN                   REG_VAL(0x01, 0)  // device must be in power down state to change this
#define INT_SOURCE              (0x25)
  #define BOOT_STATUS             REG_VAL(0x01, 7)
  #define IA                      REG_VAL(0x01, 2)
  #define PL                      REG_VAL(0x01, 1)
  #define PH                      REG_VAL(0x01, 0)
#define FIFO_STATUS             (0x26)
  #define FTH_FIFO                REG_VAL(0x01, 7)
  #define OVR                     REG_VAL(0x01, 6)
#define STATUS                  (0x27)
  #define T_OR                    REG_VAL(0x01, 5)
  #define P_OR                    REG_VAL(0x01, 4)
  #define T_DA                    REG_VAL(0x01, 1)
  #define P_DA                    REG_VAL(0x01, 0)
#define PRESS_OUT_XL            (0x28)
#define PRESS_OUT_L             (0x29)
#define PRESS_OUT_H             (0x2A)
#define TEMP_OUT_L              (0x2B)
#define TEMP_OUT_H              (0x2C)
#define LPFP_RES                (0x33)

#define FIFO_MODE_30_Samples    (0x3E)
#define FIFO_WATERMARK_INT      (0x80)
#define FIFO_STOP_ON_FTH        (0x70)

#define FIFO_BUFFER_SIZE        (30)

uint32_t* puiaPressureData[FIFO_BUFFER_SIZE];
uint32_t* puiaTemperatureData[FIFO_BUFFER_SIZE];

uint8_t uiFifoStatus;
uint8_t ucFIFOLoop;

volatile uint32_t vulPressureVal = 0;
volatile bool vbNewPressureVal = false;

void sensor_int_evt_handler(nrfx_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{

  switch(pin)
  {
    case(SENSOR_INT_PIN):
        //Set Flag for Fifo Full
        BLUR_SET_BIT(ucGlobalFlags1_, eGlobalFlag1_PressureFifoFull);
    break;
    
    default:
        NRF_LOG_ERROR("Unknown GPIO interrupt");
    break;
  }
}

void LPS22HB_GetReading(uint32_t * dest)
{
  *dest = puiaPressureData[0];
  vbNewPressureVal = false;
}

bool LPS22HB_IsDataReady()
{
  return(vbNewPressureVal);
}

/**
 * @brief Pressure sensor initialization.
 */
bool LPS22HB_init(void)
{
  bool retVal = true;
  uint8_t temp_byte;

  retVal &= i2c_read_register(LPS22HB_I2C_ADDR, WHO_AM_I, &temp_byte, 1);

  if(retVal)
  {
    // Set data rate
    retVal &= i2c_write_reg_byte(LPS22HB_I2C_ADDR, CTRL_REG1, ODR_10HZ);
    retVal &= i2c_read_register(LPS22HB_I2C_ADDR, CTRL_REG1, &temp_byte, 1);
    
    // Enable FIFO, Stop on Watermark, Address increment
    retVal &= i2c_write_reg_byte(LPS22HB_I2C_ADDR, CTRL_REG2, FIFO_EN | STOP_ON_FTH | IF_ADD_INC);
    retVal &= i2c_read_register(LPS22HB_I2C_ADDR, CTRL_REG2, &temp_byte, 1);

    // Enable Watermark interrupt generation
    retVal &= i2c_write_reg_byte(LPS22HB_I2C_ADDR, CTRL_REG3, F_FTH);
    retVal &= i2c_read_register(LPS22HB_I2C_ADDR, CTRL_REG3, &temp_byte, 1);

    // Set FIFO mode and watermark level to 30
    retVal &= i2c_write_reg_byte(LPS22HB_I2C_ADDR, FIFO_CTRL, FIFO_MODE_30_Samples);
    retVal &= i2c_read_register(LPS22HB_I2C_ADDR, FIFO_CTRL, &temp_byte, 1);

    // enable overall interrupt generation
    retVal &= i2c_write_reg_byte(LPS22HB_I2C_ADDR, INTERRUPT_CFG, DIFF_EN | LIR);
    retVal &= i2c_read_register(LPS22HB_I2C_ADDR, INTERRUPT_CFG, &temp_byte, 1);
  }

  // Check if gpiote is already initialized (it is also checked in gpio_handler)
  if (!nrf_drv_gpiote_is_init())
  {
      APP_ERROR_CHECK(nrf_drv_gpiote_init());
  }

  // interrupt pin setup
  nrfx_err_t err_code;
  err_code = nrfx_gpiote_in_init(SENSOR_INT_PIN, &m_gpiote_input_cfg, sensor_int_evt_handler);
  APP_ERROR_CHECK(err_code);

  nrfx_gpiote_in_event_enable(SENSOR_INT_PIN, true);

  //sensor_int_evt_handler(SENSOR_INT_PIN, GPIOTE_CONFIG_POLARITY_LoToHi);

  if(retVal)
  {
      NRF_LOG_INFO("Pressure sensor started.");
  }
  else
  {
      NRF_LOG_INFO("Pressure sensor failed to initialize.");
  }

  NRF_LOG_FLUSH();

  return(retVal);
}

bool LPS22HB_read(uint32_t * pDest)
{
  bool retVal = false;

  retVal = i2c_read_register(LPS22HB_I2C_ADDR, PRESS_OUT_XL, (uint8_t *)pDest, 3);
  
  if(!retVal)
  {
    NRF_LOG_INFO("Read fail");
    NRF_LOG_FLUSH();  
  }
  
  return(retVal);
}

bool LSP22HB_read_fifo()
{
    bool retVal = false;
    
    for(int i = 0; i < FIFO_BUFFER_SIZE; i++)
    {
        retVal = i2c_read_register(LPS22HB_I2C_ADDR, PRESS_OUT_XL, &puiaPressureData[i], 3);
        retVal &= i2c_read_register(LPS22HB_I2C_ADDR, TEMP_OUT_L, &puiaTemperatureData[i], 2);
    }

    return(retVal);
}

bool LSP22HB_clear_fifo()
{
    bool retVal = false;
    uint8_t tempByte;

    //READ INT_SOURCE TO CLEAR INT???
    retVal = i2c_read_register(LPS22HB_I2C_ADDR, INT_SOURCE, &tempByte, 1);

    //Reset FIFO - Datasheets says to switch to BYP then back to FIFO mode
    retVal &= i2c_write_reg_byte(LPS22HB_I2C_ADDR, FIFO_CTRL, FIFO_BYP);
    retVal &= i2c_write_reg_byte(LPS22HB_I2C_ADDR, FIFO_CTRL, FIFO_MODE_30_Samples);

    return(retVal);
}

void LPS22HB_GetFifo(uint32_t* pucaBuf)
{
    for(ucFIFOLoop = 0; ucFIFOLoop < FIFO_BUFFER_SIZE; ucFIFOLoop++)
    {
        pucaBuf[ucFIFOLoop] = puiaPressureData[ucFIFOLoop];
    }
}