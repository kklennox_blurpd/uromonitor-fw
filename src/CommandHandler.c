/* ========================================
 *
 * Copyright Blur PD 2020
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF BLUR PD.
 *
 * ========================================
*/
#include "CommandHandler.h"
#include "BlurPacketProtocol.h"

uint32_t uiPressure;
//Timestep_t _sTimestep;
//Timestep_t _Timestep;
uint16_t uiStep;
uint16_t uiCurrentStep;

enum eCommandIDs
{
    eCommandID_Versions = 0,
    eCommandID_Status,
    eCommandID_RequestDataDump,
    eCommandID_FlashErase,
    eCommandID_GetTimestep,
    eCommandID_GetNumTimestepsStored
};

//Local Functions
bool commandHandlerHasCompleteIncomingCommand();
void commandHandlerExecuteVersionsCommand();
void commandHandlerExecuteStatusCommand();
void commandHandlerExecuteDataDumpCommand();
void commandHandlerExecuteFlashErase();
void commandHandlerExecuteGetTimestep(uint16_t num_timestep);
void commandHandlerExecuteGetNumTimesteps();



void commandHandlerInit()
{
    blurPPInitializeBuffers(UARTHandlerGetSerialBuffer(),UART_BUFFER_SIZE,UARTHandlerGetSerialBuffer(),UART_BUFFER_SIZE);
    uiCurrentStep = 0;
}

uint16_t commandHandlerGetResponseByteCount()
{
    return(blurPPOutgoingPacketSize());
}

bool commandHandlerHasCompleteIncomingCommand()
{
    //Ask the packet protocol library to scan the bytes received and determine if a message terminator is present. This indicates a packet has been received
    return(blurPPTerminatorInReceiveBuffer(UARTHandlerGetInputByteCount()));
}

void commandHandlerProcessIncomingCommand()
{
    eBPPPacketParseErrors eError;

    uint16_t uiInputByteCount;

    uiInputByteCount = UARTHandlerGetInputByteCount();

    eError = blurPPValidateIncomingPacket(uiInputByteCount);

    if(eBPPPacketParseError_None == eError)
    {
        //Good packet, execute the command
        switch(blurPPIncomingCommandID())
        {
            case eCommandID_Versions:
                commandHandlerExecuteVersionsCommand();
                break;
            case eCommandID_Status:
                commandHandlerExecuteStatusCommand();
                break;
            case eCommandID_RequestDataDump:
                commandHandlerExecuteDataDumpCommand();
                break;
            case eCommandID_FlashErase:
                commandHandlerExecuteFlashErase();
                break;
            case eCommandID_GetTimestep:
                uiStep = blurPPInputDataShort();
                commandHandlerExecuteGetTimestep(uiStep);
                break;
            case eCommandID_GetNumTimestepsStored:
                commandHandlerExecuteGetNumTimesteps();
                break;
        }
    }
    else
    {   
        blurPPBeginNewPacket(1, BLURPP_DATATYPE_BINARY);
        blurPPNewErrorPacket(eError);
        blurPPTerminatePacket();
    }
}

void commandHandlerExecuteVersionsCommand()
{
    blurPPBeginNewPacket(eCommandID_Versions, BLURPP_DATATYPE_BINARY);
    
    blurPPOutputDataByte(SW_REV_MAJOR);
    blurPPOutputDataByte(SW_REV_MINOR);
    blurPPOutputDataByte(SW_REV_BUILD);

    blurPPTerminatePacket();
}

void commandHandlerExecuteStatusCommand()
{
    LPS22HB_GetReading(&uiPressure);
    
    blurPPBeginNewPacket(eCommandID_Status, BLURPP_DATATYPE_BINARY);

    blurPPOutputDataInt(uiPressure);

    blurPPTerminatePacket();

}

void commandHandlerExecuteDataDumpCommand()
{
    Timestep_t _Timestep;
    uint32_t num_timesteps;

    num_timesteps = flashScanForLastTimestep();

    blurPPBeginNewPacket(eCommandID_RequestDataDump, BLURPP_DATATYPE_BINARY);

    while(uiCurrentStep < num_timesteps)
    {
        flashReadTimestepData(uiCurrentStep, &_Timestep);

        blurPPOutputDataInt(_Timestep.uiTimeStamp);

        blurPPOutputDataShort(_Timestep.uiSequenceNum);

        blurPPOutputDataByte(_Timestep.ucTimeStepFlags);

        //Pressure Data
        for(int i = 0; i < FIFO_SIZE; i++)
        {
            blurPPOutputDataInt(_Timestep.uiPressure[i]);
        }

        blurPPOutputDataShort(_Timestep.iVbatt);

        blurPPOutputDataByte(_Timestep.ucChecksum);

        uiCurrentStep++;
    }

    blurPPTerminatePacket();

}


void commandHandlerExecuteFlashErase()
{
    bool bOK;

    bOK = flashChipErase();

    uiCurrentStep = 0;

    blurPPBeginNewPacket(eCommandID_FlashErase, BLURPP_DATATYPE_BINARY);

    blurPPOutputDataByte(bOK);

    blurPPTerminatePacket();
}


void commandHandlerExecuteGetTimestep(uint16_t num_timestep)
{
    Timestep_t tStep;

    flashReadTimestepData(num_timestep, &tStep);

    blurPPBeginNewPacket(eCommandID_GetTimestep, BLURPP_DATATYPE_BINARY);

    blurPPOutputDataInt(tStep.uiTimeStamp);

    blurPPOutputDataShort(tStep.uiSequenceNum);

    blurPPOutputDataByte(tStep.ucTimeStepFlags);

    for(int i = 0; i < FIFO_SIZE; i++)
    {
        blurPPOutputDataInt(tStep.uiPressure[i]);
    }

    blurPPOutputDataShort(tStep.iVbatt);

    blurPPOutputDataByte(tStep.ucChecksum);

    blurPPTerminatePacket();
}


void commandHandlerExecuteGetNumTimesteps()
{
    
    uint32_t num_timesteps;

    num_timesteps = flashGetLocalSteps();//flashScanForLastTimestep();

    blurPPBeginNewPacket(eCommandID_GetNumTimestepsStored, BLURPP_DATATYPE_ASCII);

    blurPPOutputDataInt(num_timesteps);

    blurPPTerminatePacket();
    
}


/* [] END OF FILE */