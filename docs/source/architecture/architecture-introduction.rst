############
Introduction
############

The Uromonitor system is a wearable device with two main components:

1) Uromonitor - a disposable, wearable device that reads and logs data 
    to persistent flash memory from a pressure transducer and presents information after a session for download via BLE.
2) Mobile application - connects to the Uromonitor via Bluetooth Low Energy, 
    and allows for user management, device configuration, and data download.


*************************
User-Device Interfaces
*************************

The Uromonitor has the following interfaces which allow a user to physically interact with the device. 

LEDs
~~~~

One multi-color LED exists on the Uromonitor, 
which allows for x,y,z.
See the :ref:`LED module <module-leds>` for detailed information.

Button
~~~~~~~~~~~~~

A button exists on the Uromonitor, which allows for x,y,z. 
See the :ref:`GPIO module <module-gpio>` for detailed information.

External Interfaces
*******************

Bluetooth Low Energy
~~~~~~~~~~~~~~~~~~~~

UART
~~~~

SWD
~~~

