/* ========================================
 *
 * Copyright BlurPD, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur PD 2022
 *
 * ========================================
*/

#include "UARTHandler.h"
#include <string.h>

#define MAX_TEST_DATA_BYTES     (15U)                /**< max number of test bytes to be used for tx and rx. */
#define UART_TX_BUF_SIZE 1024                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE 1024                         /**< UART RX buffer size. */

//P0.05 - RTS
//P0.06 - TXD
//P0.07 - CTS
//P0.08 - RXD
#define RX_PIN_NUMBER  22
#define TX_PIN_NUMBER  23
#define CTS_PIN_NUMBER 7
#define RTS_PIN_NUMBER 5
#define HWFC           true

static nrf_drv_uart_t app_uart_inst = NRF_DRV_UART_INSTANCE(APP_UART_DRIVER_INSTANCE);

//Local variables
uint8_t ucaUARTBuffer[UART_BUFFER_SIZE];  //Array to hold the I/O data
uint16_t uiUARTInputIndex;  //Number of characters received

bool bUARTIsSleeping = false;
uint8_t ucEvent;
uint16_t test;

enum eUARTEvents
{
    eUARTEvent_COMM_ERROR = 0,
    eUARTEvent_FIFO_ERROR,
    eUARTEvent_TX_EMPTY,
    eUARTEvent_DATA_READY,
    eUARTEvent_DATA_RECEIVED
};

//Local Functions
void UARTHandler_Put(char ucByte);

void uart_error_handle(app_uart_evt_t * p_event)
{
    if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_communication);
        //ucEvent = eUARTEvent_COMM_ERROR;
    }
    else if (p_event->evt_type == APP_UART_FIFO_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_code);
        //ucEvent = eUARTEvent_FIFO_ERROR;
    }
    else if (p_event->evt_type == APP_UART_TX_EMPTY)
    {
        ucEvent = eUARTEvent_TX_EMPTY;
    }
    else if(p_event->evt_type == APP_UART_DATA_READY)
    {
        ucEvent = eUARTEvent_DATA_READY;
    }
    else if(p_event->evt_type == APP_UART_DATA)
    {
        //printf("UART DATA");
        //ucEvent = eUARTEvent_DATA_RECEIVED;
    }
}

void UARTHandlerInit()
{
    uint32_t err_code;

    //Reset
    memset(ucaUARTBuffer,0,UART_BUFFER_SIZE);
    uiUARTInputIndex = 0;

    //NORDIC INIT STUFF
    const app_uart_comm_params_t comm_params =
      {
          RX_PIN_NUMBER,
          TX_PIN_NUMBER,
          RTS_PIN_NUMBER,
          CTS_PIN_NUMBER,
          UART_HWFC,
          false,
#if defined (UART_PRESENT)
          NRF_UART_BAUDRATE_115200
#else
          NRF_UARTE_BAUDRATE_115200
#endif
      };

      APP_UART_FIFO_INIT(&comm_params,
                         UART_RX_BUF_SIZE,
                         UART_TX_BUF_SIZE,
                         uart_error_handle,
                         APP_IRQ_PRIORITY_LOWEST,
                         err_code);

    APP_ERROR_CHECK(err_code);
    
}

uint8_t* UARTHandlerGetSerialBuffer()
{
    return(ucaUARTBuffer);
}

uint16_t UARTHandlerGetInputByteCount()
{
    return(uiUARTInputIndex);
}

void UARTHanlderReset()
{
    //Reset RAM buffer to empty
    memset(ucaUARTBuffer,0,UART_BUFFER_SIZE);
    //Reset input count to zero
    uiUARTInputIndex = 0;
    //Clear the FIFOs for next command
    app_uart_flush();
}

void UARTHandlerWriteBuffer(uint16_t uiCount)
{
    uint32_t uiSent;
    uint16_t uiIndex = 0;
    bool bInProgress;

    //Loop over the size of the output buffer
    while(uiCount > 0)
    {
        //Attempt to put byte into the TX Fifo
        uiSent = app_uart_put(ucaUARTBuffer[uiIndex]);
        //Did the byte going into the FIFO?
        if(NRF_SUCCESS == uiSent)
        {
            //Byte went into FIFO, increment index and decrement count
            uiIndex++;
            uiCount--;
        }
    }

    //Loop until TX Fifo is empty
    do
    {
        //Anything in TX FIFO
        bInProgress = nrf_drv_uart_tx_in_progress(&app_uart_inst);
    }
    while(true == bInProgress);
}

void UARTHandlerProcessEvents()
{
    uint32_t uiData;
    uint8_t ucByteReceived;
    
    /*
    if(UART_TEST_MODE == nrf_gpio_pin_read(UART_ENABLE_PIN))
    {
        if(bUARTIsSleeping)
        {
            //UARTWAKE
            UARTHandlerInit();
            bUARTIsSleeping = false;
        }
        
            //Attempt to read data from RX Fifo
        do
        {
        //Try to get a byte from FIFO
        uiData = app_uart_get(&ucByteReceived);
        //Did we get a byte?
        if(NRF_SUCCESS == uiData)
        {
            
            //Prevent buffer overrun
            if(uiUARTInputIndex >= UART_BUFFER_SIZE)
            {
                uiUARTInputIndex = 0;
            }
            //Store incoming byte in RAM buffer
            ucaUARTBuffer[uiUARTInputIndex] = ucByteReceived; //& 0x000000FFu;
 
            //Increment stored byte count
            uiUARTInputIndex++;
        }
    }
    while(NRF_SUCCESS == uiData); //Loop while we have data in FIFO
        
    }
    else
    {
        if(!bUARTIsSleeping)
        {
            //UART SLEEP
            UARTHandlerClose();
            bUARTIsSleeping = true;
        }
    }
    */
    
    //Attempt to read data from RX Fifo
    do
    {
        //Try to get a byte from FIFO
        uiData = app_uart_get(&ucByteReceived);
        //Did we get a byte?
        if(NRF_SUCCESS == uiData)
        {
            
            //Prevent buffer overrun
            if(uiUARTInputIndex >= UART_BUFFER_SIZE)
            {
                uiUARTInputIndex = 0;
            }
            //Store incoming byte in RAM buffer
            ucaUARTBuffer[uiUARTInputIndex] = ucByteReceived; //& 0x000000FFu;
 
            //Increment stored byte count
            uiUARTInputIndex++;

        }
    }
    while(NRF_SUCCESS == uiData); //Loop while we have data in FIFO
}

void UARTHandlerClose()
{
    app_uart_close();
}
/* [] END OF FILE */

