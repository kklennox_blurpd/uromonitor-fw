:: Based heavily off of https://gitlab.com/wolframroesler/version/
:: And https://embeddedartistry.com/blog/2016/12/21/giving-your-firmware-build-a-version/
:: Modified for Windows batch file due to PSoC Creator being Windows-only. 
:: Expects tag to follow format of <MAJOR>.<MINOR>

:: USAGE:
:: Set git tag to <major>.<minor>.<build>, e.g. 2.4.154
:: Include BlurAutoVersion.h in your PSoC Creator header files for the project
:: Add BlurAutoVersion.bat as a pre-build User Command:
::	Right click project -> ARM GCC -> User Commands

@ECHO OFF

:: Get the git repository version number
::git describe --long --dirty --tags --always
FOR /F %%i IN ('git describe --long --dirty --tags --always') DO set GIT_VERS=%%i

:: See https://ss64.com/nt/for_f.html
FOR /F "tokens=1,2,3,4,5* delims=.-" %%G IN ("%GIT_VERS%") DO (
SET "VER_MAJOR=%%G"
SET "VER_MINOR=%%H"
SET "VER_BUILD=%%I"
SET "VER_BEHIND=%%J"
SET "VER_COMMIT=%%K"
SET "VER_DIRTY=%%L"
)

:: Find and replace 'dirty' with '+', if it exists
set str_find=dirty
set str_replace=+
call set VER_DIRTY=%%VER_DIRTY:%str_find%=%str_replace%%%
if NOT "%VER_DIRTY%"=="+" SET "VER_DIRTY="

@echo %VER_DIRTY%

>BlurAutoVersion.h (
	@ECHO:/^* 
	@ECHO:^* ========================================
	@ECHO:^* Copyright Blur Product Development, 2018
	@ECHO:^* All Rights Reserved
	@ECHO:^* UNPUBLISHED, LICENSED SOFTWARE.
	@ECHO:^*
	@ECHO:^* CONFIDENTIAL AND PROPRIETARY INFORMATION
	@ECHO:^* WHICH IS THE PROPERTY OF Blur Product Development.
	@ECHO:^*
	@ECHO:^* ========================================
	@ECHO:^*/
	@ECHO.
	@ECHO.
	@ECHO:/^*
	@ECHO:^* ========================================
	@ECHO:^* 	DO NOT MODIFY, AUTO-GENERATED.			
	@ECHO:^* ========================================
	@ECHO:^* USAGE:
	@ECHO:^* Set git tag to ^<major^>.^<minor^>.^<build^>, e.g. 2.4.154
	@ECHO:^* Include BlurAutoVersion.h in your PSoC Creator header files for the project
	@ECHO:^* Add BlurAutoVersion.bat as a pre-build User Command:
	@ECHO:^* 	Right click project -^> ARM GCC -^> User Commands
	@ECHO:^*/
	@ECHO.
	@ECHO:#ifndef BLUR_AUTOVERSION_H
	@ECHO:#define BLUR_AUTOVERSION_H
	@ECHO. 
	@ECHO:#include ^<project.h^>
	@ECHO. 
	@ECHO:	#define VER_MAJOR 		(%VER_MAJOR%^)
	@ECHO:	#define VER_MINOR 		(%VER_MINOR%^)
	@ECHO:	#define VER_BUILD 		(%VER_BUILD%^)
	@ECHO:	#define VER_BEHIND 		(%VER_BEHIND%^)
	@ECHO:	#define VER_COMMIT 		("%VER_COMMIT%"^)
	@ECHO:	#define VER_DIRTY 		("%VER_DIRTY%"^) 
	@ECHO:	#define VER_FULL 		("%VER_COMMIT%%VER_DIRTY%"^)
	@ECHO. 
	@ECHO:#endif //BLUR_AUTOVERSION_H
	@ECHO:/^* [] END OF FILE ^*/
)

:: Output the version number to the build log
::echo "Building version $VERSION" >&2
@ECHO GIT DESCRIBE OUTPUT: %GIT_VERS%
@ECHO VER_MAJOR: %VER_MAJOR%
@ECHO VER_MINOR: %VER_MINOR%
@ECHO VER_BUILD: %VER_BUILD%
@ECHO VER_BEHIND: %VER_BEHIND%
@ECHO VER_COMMIT: %VER_COMMIT%
@ECHO VER_DIRTY: %VER_DIRTY%



