/* ========================================
 *
 * Copyright Blur Product Development, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur Product Development.
 *
 * ========================================
*/

#ifndef GPIO_HANDLER_H
#define GPIO_HANDLER_H

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "nrf_gpio.h" 

void gpio_handler_init(void);
void enable_gpio_interrupts(void);

void pwr_ctrl_on(void);
void pwr_ctrl_off(void);
bool button_active();

#endif //GPIO_HANDLER_H