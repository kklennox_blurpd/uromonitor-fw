/* ========================================
 *
 * Copyright Blur Product Development, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur Product Development.
 *
 * ========================================
*/

/**
 * @brief Handles I2C initialization, event handling, and bus reads/writes. 
 *
 * This file acts as the clearinghouse for I2C activities. It integrates with other handlers for external peripherals.
 */

#include "i2c_handler.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "sdk_config.h"

#include "nrf_drv_twi.h"

#include "app_util_platform.h"
#include "app_error.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"

#define BOARD_SCL_PIN             16    // SCL signal pin
#define BOARD_SDA_PIN             18    // SDA signal pin

/* TWI instance ID. */
#if TWI0_ENABLED
#define TWI_INSTANCE_ID     0
#elif TWI1_ENABLED
#define TWI_INSTANCE_ID     1
#endif

 /* Number of possible TWI addresses. */
 #define TWI_ADDRESSES      127

/* TWI instance. */
static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);


/**
 * @brief I2C initialization.
 */
void i2c_init (void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t twi_config = {
       .scl                = BOARD_SCL_PIN,
       .sda                = BOARD_SDA_PIN,
       .frequency          = NRF_DRV_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = false
    };

    err_code = nrf_drv_twi_init(&m_twi, &twi_config, NULL, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi);

    NRF_LOG_INFO("I2C driver started.");
    NRF_LOG_FLUSH();
}

void i2c_scan(void)
{
  ret_code_t err_code;
  uint8_t address;
  uint8_t sample_data;
  bool detected_device = false;

  for (address = 1; address <= TWI_ADDRESSES; address++)
    {
        err_code = nrf_drv_twi_rx(&m_twi, address, &sample_data, sizeof(sample_data));
        if (err_code == NRF_SUCCESS)
        {
            detected_device = true;
            NRF_LOG_INFO("TWI device detected at address 0x%x.", address);
        }
        NRF_LOG_FLUSH();
    }

    if (!detected_device)
    {
        NRF_LOG_INFO("No device was found.");
        NRF_LOG_FLUSH();
    }
}

bool i2c_write_reg_byte(uint8_t ucAddress, uint8_t ucRegister, uint8_t ucByte)
{
  ret_code_t err_code;
  uint8_t data_temp[2] = {ucRegister, ucByte};

  err_code = nrf_drv_twi_tx(&m_twi, ucAddress, &data_temp, 2, false);
  if (err_code == NRF_SUCCESS)
  {
    return(true);
  }
  return(false);
}

bool i2c_read_register(uint8_t ucAddress, uint8_t ucRegister, uint8_t * pDest, uint8_t ucBytesToRead)
{
  ret_code_t err_code;
  err_code = nrf_drv_twi_tx(&m_twi, ucAddress, &ucRegister, 1, true);
  if (err_code == NRF_SUCCESS)
  {
      err_code = nrf_drv_twi_rx(&m_twi, ucAddress, pDest, ucBytesToRead);
      if (err_code == NRF_SUCCESS)
      {
        return(true);
      }
  }

  return(false);
}