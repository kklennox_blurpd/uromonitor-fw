/* ========================================
 *
 * Copyright BlurPD, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur PD 2022
 *
 * ========================================
*/

#include "RTCInterface.h"


#define COMPARE_COUNTERTIME  (1UL) 

const nrf_drv_rtc_t rtc = NRF_DRV_RTC_INSTANCE(0); 

static void rtc_handler(nrf_drv_rtc_int_type_t int_type)
{
    if (int_type == NRF_DRV_RTC_INT_COMPARE0)
    {
        BLUR_SET_BIT(ucGlobalFlags1_, eGlobalFlag1_TimerTick);
    }
    else if (int_type == NRF_DRV_RTC_INT_TICK)
    {
        LOG(Tick);
    }
}

void RTCInterfaceInit()
{
    uint32_t err_code;

    //Initialize RTC instance
    nrf_drv_rtc_config_t config = NRF_DRV_RTC_DEFAULT_CONFIG;
    config.prescaler = 4095; //8Hz
    err_code = nrf_drv_rtc_init(&rtc, &config, rtc_handler);
    APP_ERROR_CHECK(err_code);

    //Enable tick event & interrupt
    //nrf_drv_rtc_tick_enable(&rtc,true);

    //Set compare channel to trigger interrupt after COMPARE_COUNTERTIME seconds
    err_code = nrf_drv_rtc_cc_set(&rtc, 0, COMPARE_COUNTERTIME * 8, true);
    APP_ERROR_CHECK(err_code);

    //Power on RTC instance
    nrf_drv_rtc_enable(&rtc);
}


uint32_t RTCInterfaceGetCounts()
{
    uint32_t uiCounts;

    uiCounts = nrfx_rtc_counter_get(&rtc);

    return(uiCounts);
}

void RTCInterfaceClearCompare0Int()
{
    //nrf_rtc_int_disable(&rtc, NRF_RTC_INT_COMPARE0_MASK);
    //nrf_rtc_int_enable(&rtc, NRF_RTC_INT_COMPARE0_MASK);
    nrf_drv_rtc_counter_clear(&rtc);
    nrf_drv_rtc_int_enable(&rtc, NRF_RTC_INT_COMPARE0_MASK);
}
/* [] END OF FILE */

