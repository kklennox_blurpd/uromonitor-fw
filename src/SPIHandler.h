/* ========================================
 *
 * Copyright BlurPD, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur PD 2022
 *
 * ========================================
*/

#ifndef SPIHANDLER_H
#define SPIHANDLER_H

#include "globals.h"
#include "nrf_drv_spi.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#define SPI_CS_PIN 6
#define SPI_CLK_PIN 9
#define SPI_MOSI_PIN 10
#define SPI_MISO_PIN 12


extern void SPIHandlerInit();
bool SPIisInitialized();
extern void SPIReset();
//Functions assume caller has configured the appropriate chip select line
extern void SPIWriteByte(uint8_t ucData);
extern void SPIWriteBuffer(uint8_t*,uint16_t uiCount);
extern void SPIWriteBufferFast(uint8_t*,uint16_t);
extern uint8_t SPIReadByte();
extern void SPIReadBuffer(uint8_t*,uint16_t uiCount);
extern void SPIReadBufferFast(uint8_t*,uint8_t);

#endif
/* [] END OF FILE */
