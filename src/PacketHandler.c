/* ========================================
 *
 * Copyright Blur Product Development, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur Product Development.
 *
 * ========================================
*/

//Includes
#include "PacketHandler.h"

Timestep_t stTimestepPacket;
uint32_t uiPressureData[FIFO_SIZE];
uint8_t ucPacketLoop;

void PacketHandlerInit()
{
    memset(&stTimestepPacket,0,sizeof(p_Timestep_t));
}

void PacketHandlerConstructTimestepPacket()
{
    stTimestepPacket.uiTimeStamp += 3; //placeholder
    stTimestepPacket.uiSequenceNum += 1;//placeholder
    stTimestepPacket.ucTimeStepFlags = eTimeStepFlag_Sensor_Reading;

    LPS22HB_GetFifo(uiPressureData);
    for(ucPacketLoop = 0; ucPacketLoop < FIFO_SIZE; ucPacketLoop++)
    {
        stTimestepPacket.uiPressure[ucPacketLoop] = uiPressureData[ucPacketLoop];
    }
    
    stTimestepPacket.iVbatt = 2;//placeholder
    stTimestepPacket.ucChecksum = 10;//placeholder
}

void PacketHandlerConstructLogPacket()
{
    stTimestepPacket.uiTimeStamp += 3; //placeholder
    stTimestepPacket.uiSequenceNum += 1;//placeholder
    stTimestepPacket.ucTimeStepFlags = eTimeStepFlag_Log;

    LPS22HB_GetFifo(uiPressureData);
    for(ucPacketLoop = 0; ucPacketLoop < FIFO_SIZE; ucPacketLoop++)
    {
        stTimestepPacket.uiPressure[ucPacketLoop] = uiPressureData[ucPacketLoop];
    }
    
    stTimestepPacket.iVbatt = 2;//placeholder
    stTimestepPacket.ucChecksum = 10;//placeholder
}

p_Timestep_t PacketHandlerGetTimestepPacket()
{
    return(&stTimestepPacket);
}

p_Timestep_t PacketHandlerGetLogPacket()
{
    return(&stTimestepPacket);
}


uint8_t PacketHandlerCalculateChecksum(Timestep_t * pInfo)
{
    uint8_t ucChecksum = 0;
    
    ucChecksum = pInfo->uiTimeStamp;
    ucChecksum += pInfo->uiSequenceNum;
    ucChecksum += pInfo->ucTimeStepFlags;
    ucChecksum += pInfo->iVbatt;

    for(int i = 0; i < FIFO_SIZE; i++)
    {
        ucChecksum += pInfo->uiPressure[i];
    }

    return(ucChecksum);

}











/* [] END OF FILE */
