/* ========================================
 * Source: Memfault post on CLI: https://interrupt.memfault.com/blog/firmware-shell
 * https://github.com/memfault/interrupt/tree/master/example/firmware-shell
*/

#include "BlurCLI.h"

#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include "stdlib.h"

#include "project.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "SEGGER_RTT.h"

#include "BlurAutoVersion.h" // for ID command

int cli_cmd_id(int argc, char *argv[]){
    // We expect 1 arguments:
    // 1. cmd name (id)
    if (argc != 1) {
        shell_put_line("> FAIL,1");
        return(-1);
    }

    SEGGER_RTT_printf(CLI_RTT_TERMINAL_NUM, "> ID,nRF52811,%u.%u.%u.%u-%s\r\n",VER_MAJOR, VER_MINOR, VER_BUILD, VER_BEHIND, VER_FULL); 
    return(0);
}

int cli_cmd_mac(int argc, char *argv[]){
    // We expect 1 arguments:
    // 1. cmd name (mac)
    if (argc != 1) {
        shell_put_line("> FAIL,1");
        return(-1);
    }

    SEGGER_RTT_printf(CLI_RTT_TERMINAL_NUM, "> MAC,\n"); 
    return(0);
}


static const sShellCommand s_shell_commands[] = {
    {"id", cli_cmd_id, "Display system information"},
    {"mac", cli_cmd_mac, "Display device MAC address"},
    {"help", shell_help_handler, "Lists all commands"},
};

const sShellCommand *const g_shell_commands = s_shell_commands;
const size_t g_num_shell_commands = ARRAY_SIZE(s_shell_commands);

/* [] END OF FILE */
