Flash
#####

.. _module-flash:

The SmartLink contains a flash memory IC to store new firmware images and logged timestep data for retrieval via BLE.

