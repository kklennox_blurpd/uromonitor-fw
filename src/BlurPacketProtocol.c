/* ========================================
 *
 * Copyright Blur PD 2020
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF BLUR PD.
 *
 * ========================================
*/
#include "BlurPacketProtocol.h"
#include <string.h>
#include <stdio.h>

//Local variables
unsigned char *pucaBPPTransmitBuffer;
unsigned int uiBPPTransmitBufferSize;
unsigned char *pucaBPPReceiveBuffer;
unsigned int uiBPPReceiveBufferSize;
unsigned char ucBPPValidationFailureDebugData;
unsigned char ucBPPIncomingCommandID;
unsigned char ucBPPIncomingSourceID;
unsigned char ucBPPIncomingDestinationID;
unsigned short int uiBPPIncomingSequenceNumber;
unsigned char ucBPPIncomingDataType;
unsigned int uiBPPIncomingDataPointerIndex;
unsigned int uiBPPIncomingDataByteCount;
unsigned int uiBPPIncomingDataEndIndex;
unsigned int uiBPPIncomingTotalPacketSize;
unsigned char ucBPPOutgoingSourceID;
unsigned char ucBPPOutgoingDestinationID;
unsigned short int uiBPPOutgoingSequenceNumber;
unsigned char ucBPPOutgoingDataType;
unsigned int uiBPPOutgoingDataPointerIndex;
unsigned int uiBPPOutgoingTotalPacketSize;

//Local functions
unsigned char blurPPValidateASCIIHex(unsigned char *pucaData, unsigned int uiCount);
unsigned char blurPPASCIIHexToByte(unsigned char *pucaData);
unsigned short int blurPPASCIIHexToShort(unsigned char *pucaData);
unsigned char blurPPCalcChecksum(unsigned char *pucaData, unsigned int uiCount);
void blurPPByteToASCII(unsigned char ucData, unsigned char *pucaData);
void blurPPShortToASCII(unsigned short int uiData, unsigned char *pucaData);

#define CMD_START       0x24u    //$
#define CMD_TERM1       0x0Du   //carriage return
#define CMD_TERM2       0x0Au   //line feed
#define CMD_DATA_END    0x26u   //&
#define CMD_ESCAPE      0x1Bu   //escape
#define CMD_NAK         0x15u   //NAK

#define MIN_PACKET_INPUT_SIZE   0x00000015u   //An empty packet with no data contains 21 bytes
#define FIRST_DATA_BYTE_INDEX   0x00000010u   //First data byte is at index 16

extern void blurPPInitializeBuffers(unsigned char *pucaTransmitBuffer, unsigned int uiTransmitBufferSize, unsigned char *pucaReceiveBuffer, unsigned int uiReceiveBufferSize)
{
    pucaBPPTransmitBuffer = pucaTransmitBuffer;
    uiBPPTransmitBufferSize = uiTransmitBufferSize;
    pucaBPPReceiveBuffer = pucaReceiveBuffer;
    uiBPPReceiveBufferSize = uiReceiveBufferSize;
    
    ucBPPIncomingCommandID = 0;
    ucBPPIncomingSourceID = 0;
    ucBPPIncomingDestinationID = 0;
    uiBPPIncomingSequenceNumber = 0;
    ucBPPIncomingDataType = 0;
    uiBPPIncomingDataPointerIndex = 0;
    uiBPPIncomingDataByteCount = 0;
    uiBPPIncomingDataEndIndex = 0;
    uiBPPIncomingTotalPacketSize = 0;
    
    ucBPPOutgoingSourceID = 0;
    ucBPPOutgoingDestinationID = 0;
    uiBPPOutgoingSequenceNumber = 0;
    ucBPPOutgoingDataType = 0;
    uiBPPOutgoingDataPointerIndex = 0;
    uiBPPOutgoingTotalPacketSize = 0;
}

unsigned char blurPPTerminatorInReceiveBuffer(unsigned int uiInputCount)
{
    unsigned char ucRetVal = 0;
    unsigned int uiLoop;
    unsigned int uiTerm1Index = 0;
    
    //Look for TERM1 byte
    for(uiLoop = 0;(uiLoop < uiInputCount) && (uiLoop < uiBPPReceiveBufferSize);uiLoop++)
    {
        if(CMD_TERM1 == pucaBPPReceiveBuffer[uiLoop])
        {
            uiTerm1Index = uiLoop;
            break;
        }
    }
    //If we found a TERM1, the next byte must be a TERM2
    if(uiTerm1Index > 0)
    {
        //Protect against buffer overrun
        if(uiBPPTransmitBufferSize > (uiTerm1Index + 1))
        {
            if(CMD_TERM2 == pucaBPPReceiveBuffer[uiTerm1Index + 1])
            {
                ucRetVal = 1;
            }
        }
    }
    
    return(ucRetVal);
}

eBPPPacketParseErrors blurPPValidateIncomingPacket(unsigned int uiInputCount)
{
    eBPPPacketParseErrors eError = eBPPPacketParseError_None;
    unsigned short int uiPacketSizeFromPacket;
    unsigned char ucChecksumFromPacket;
    unsigned char ucCalcCheckSum;
    
    //Reset variables
    ucBPPValidationFailureDebugData = 0;
    ucBPPIncomingCommandID = 0;
    ucBPPIncomingSourceID = 0;
    ucBPPIncomingDestinationID = 0;
    uiBPPIncomingSequenceNumber = 0;
    ucBPPIncomingDataType = 0;
    uiBPPIncomingDataByteCount = 0;
    uiBPPIncomingDataEndIndex = 0;
    uiBPPIncomingTotalPacketSize = 0;
    //Make sure start delimiter is first element
    if(CMD_START == pucaBPPReceiveBuffer[0])
    {
        //Ensure the input packet size it at least the size of the minimum packet, i.e. a packet with no data
        if(uiInputCount >= MIN_PACKET_INPUT_SIZE)
        {
            //Make sure the command ID field has valid ASCII hex characters
            if(0 != blurPPValidateASCIIHex(&pucaBPPReceiveBuffer[1],2))
            {
                //Extract the command ID
                ucBPPIncomingCommandID = blurPPASCIIHexToByte(&pucaBPPReceiveBuffer[1]);
                //Make sure the packet length field has all valid ASCII hex characters
                if(0 != blurPPValidateASCIIHex(&pucaBPPReceiveBuffer[3],4))
                {
                    //Extract the ASCII hex characters from the incoming message
                    uiPacketSizeFromPacket = blurPPASCIIHexToShort(&pucaBPPReceiveBuffer[3]);
                    //Make sure the packet length reported in the incoming message matches what was actually received
                    if(uiPacketSizeFromPacket == uiInputCount)
                    {
                        //Make sure the source ID field has all valid ASCII hex characters
                        if(0 != blurPPValidateASCIIHex(&pucaBPPReceiveBuffer[7],2))
                        {
                            //Extract the source ID from the incoming message
                            ucBPPIncomingSourceID = blurPPASCIIHexToByte(&pucaBPPReceiveBuffer[7]);
                            //Make sure the destination ID from incoming message has all validate ASCII hex characters
                            if(0 != blurPPValidateASCIIHex(&pucaBPPReceiveBuffer[9],2))
                            {
                                //Extract the destination ID from the incoming message
                                ucBPPIncomingDestinationID = blurPPASCIIHexToByte(&pucaBPPReceiveBuffer[9]);
                                //Make sure all the sequence field characters are valid ASCII hex chars
                                if(0 != blurPPValidateASCIIHex(&pucaBPPReceiveBuffer[11],4))
                                {
                                    //Extract the incoming sequence ID from the incoming message
                                    uiBPPIncomingSequenceNumber = blurPPASCIIHexToShort(&pucaBPPReceiveBuffer[11]);
                                    //Make sure the data type specifier is valid and store
                                    switch(pucaBPPReceiveBuffer[15])
                                    {
                                        case BLURPP_DATATYPE_ASCII:
                                        case 'a':
                                            ucBPPIncomingDataType = BLURPP_DATATYPE_ASCII;
                                            break;
                                        case BLURPP_DATATYPE_BINARY:
                                        case 'b':
                                            ucBPPIncomingDataType = BLURPP_DATATYPE_BINARY;
                                            break;
                                        case BLURPP_DATATYPE_NONE:
                                            ucBPPIncomingDataType = BLURPP_DATATYPE_NONE;
                                            break;
                                        default:
                                            eError = eBPPPacketParseError_Invalid_Data_Type;
                                            ucBPPValidationFailureDebugData = pucaBPPReceiveBuffer[15];
                                    }
                                    if(eBPPPacketParseError_None == eError)
                                    {
                                        //Make sure the data stop delimiter is at the end of the data payload
                                        if(CMD_DATA_END == pucaBPPReceiveBuffer[uiInputCount - 5])
                                        {
                                            //Store the index of the data end character for later use
                                            uiBPPIncomingDataEndIndex = uiInputCount - 5;
                                            //Make sure the checksum field has all valid ASCII characters
                                            if(0 != blurPPValidateASCIIHex(&pucaBPPReceiveBuffer[uiInputCount - 4],2))
                                            {
                                                //Extract the checksum sent in the packet
                                                ucChecksumFromPacket = blurPPASCIIHexToByte(&pucaBPPReceiveBuffer[uiInputCount - 4]);
                                                //Set the checksum bytes to zero to checksum calc will work as expected
                                                //We don't want to include the incoming checksum in our calculation
                                                pucaBPPReceiveBuffer[uiInputCount - 4] = 0;
                                                pucaBPPReceiveBuffer[uiInputCount - 3] = 0;
                                                //Calculate the checksum of the packet as sent
                                                ucCalcCheckSum = blurPPCalcChecksum(pucaBPPReceiveBuffer,uiInputCount);
                                                //Make sure checksums match
                                                if(ucCalcCheckSum == ucChecksumFromPacket)
                                                {
                                                    //Make sure first end delimiter is OK
                                                    if(CMD_TERM1 == pucaBPPReceiveBuffer[uiInputCount - 2])
                                                    {
                                                        if(CMD_TERM2 == pucaBPPReceiveBuffer[uiInputCount - 1])
                                                        {
                                                            uiBPPIncomingDataPointerIndex = FIRST_DATA_BYTE_INDEX;
                                                            uiBPPIncomingDataByteCount = uiInputCount - MIN_PACKET_INPUT_SIZE;
                                                            uiBPPIncomingTotalPacketSize = uiInputCount;
                                                        }
                                                        else
                                                        {
                                                            eError = eBPPPacketParseError_Invalid_Terminator2_Delimiter;
                                                            ucBPPValidationFailureDebugData = pucaBPPReceiveBuffer[uiInputCount - 1];
                                                        }
                                                    }
                                                    else
                                                    {
                                                        eError = eBPPPacketParseError_Invalid_Terminator1_Delimiter;
                                                        ucBPPValidationFailureDebugData = pucaBPPReceiveBuffer[uiInputCount - 2];
                                                    }
                                                }
                                                else
                                                {
                                                    eError = eBPPPacketParseError_Checksum_Mismatch;
                                                }
                                            }
                                            else
                                            {
                                                eError = eBPPPacketParseError_Invalid_Checksum;
                                            }
                                        }
                                        else
                                        {
                                            eError = eBPPPacketParseError_Invalid_Data_End_Delimiter;
                                        }
                                    }
                                }
                                else
                                {
                                    eError = eBPPPacketParseError_Invalid_Sequence_Number;
                                }
                            }
                            else
                            {
                                eError = eBPPPacketParseError_Invalid_Destination_ID;
                            }
                        }
                        else
                        {
                            eError = eBPPPacketParseError_Invalid_Source_ID;
                        }
                    }
                    else
                    {
                        eError = eBPPPacketParseError_Packet_Length_Mismatch;
                    }
                }
                else
                {
                    eError = eBPPPacketParseError_Invalid_Packet_Length_In_Packet;
                }
            }
            else
            {
                eError = eBPPPacketParseError_Invalid_Commmand_ID;
            }
        }
        else
        {
            eError = eBPPPacketParseError_Invalid_Min_Packet_Size;
        }
    }
    else
    {
        eError = eBPPPacketParseError_Invalid_Start_Delimiter;
        ucBPPValidationFailureDebugData = pucaBPPReceiveBuffer[0];
    }
    
    return(eError);
}

unsigned char blurPPIncomingCommandID()
{
    return(ucBPPIncomingCommandID);
}

unsigned char blurPPValidationFailureDebugData()
{
    return(ucBPPValidationFailureDebugData);
}

unsigned char blurPPIncomingSourceID()
{
    return(ucBPPIncomingSourceID);
}

unsigned char blurPPIncomingDestinationID()
{
    return(ucBPPIncomingDestinationID);
}

unsigned short int blurPPIncomingSequenceNumber()
{
    return(uiBPPIncomingSequenceNumber);
}

unsigned char blurPPIncomingDataType()
{
    return(ucBPPIncomingDataType);
}

extern unsigned int blurPPIncomingDataByteCount()
{
    unsigned int uiRetVal = 0;
    
    switch(ucBPPIncomingDataType)
    {
        case BLURPP_DATATYPE_ASCII:
            uiRetVal = uiBPPIncomingDataByteCount / 2;  //Each byte is sent as two ASCII hex chars, so number of bytes is total over 2
            break;
        case BLURPP_DATATYPE_BINARY:
            {
                unsigned int uiLoop;
                
                //The incoming byte count for binary mode is the total data payload size minus any ESC characters to denote transformed data
                for(uiLoop = FIRST_DATA_BYTE_INDEX;uiLoop < uiBPPIncomingDataEndIndex;uiLoop++)
                {
                    //Is the byte in the input buffer a ESC?
                    if(CMD_ESCAPE != pucaBPPReceiveBuffer[uiLoop])
                    {
                        //Not an ESC so increment the total
                        uiRetVal++;
                    }
                }
            }
            break;
    }
    
    return(uiRetVal);
}

unsigned char blurPPValidateASCIIHex(unsigned char *pucaData, unsigned int uiCount)
{
    unsigned char ucRetVal = 1;
    unsigned int uiLoop;
    
    for(uiLoop = 0;uiLoop < uiCount && 0 != ucRetVal;uiLoop++)
    {
        switch(pucaData[uiLoop])
        {
            case 'a':
                pucaData[uiLoop] = 'A';
                break;
            case 'b':
                pucaData[uiLoop] = 'B';
                break;
            case 'c':
                pucaData[uiLoop] = 'C';
                break;
            case 'd':
                pucaData[uiLoop] = 'D';
                break;
            case 'e':
                pucaData[uiLoop] = 'E';
                break;
            case 'f':
                pucaData[uiLoop] = 'F';
                break;
            case 'A':
            case 'B':
            case 'C':
            case 'D':
            case 'E':
            case 'F':
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                break;
            default:
                ucRetVal = 0;
                ucBPPValidationFailureDebugData = pucaData[uiLoop];
        }
    }
    
    return(ucRetVal);
}

unsigned char blurPPCalcChecksum(unsigned char *pucaData, unsigned int uiCount)
{
    unsigned char ucRetVal = 0;
    unsigned int uiLoop;
    
    for(uiLoop = 0;uiLoop < uiCount;uiLoop++)
    {
        ucRetVal += pucaData[uiLoop];
    }
    
    return(ucRetVal);
}

unsigned char blurPPASCIIHexToByte(unsigned char *pucaData)
{
    unsigned char ucRetVal;
    
    //Convert first digit (upper nibble)
    if((pucaData[0] >= 'A') && ('F' >= pucaData[0]))
    {
        ucRetVal = 10 + (pucaData[0] - 'A');
    }
    else
    {
        ucRetVal = pucaData[0] - '0';
    }
    ucRetVal <<= 4;
    ucRetVal &= 0xF0u;
    //Convert second digit (lower nibble)
    if((pucaData[1] >= 'A') && ('F' >= pucaData[1]))
    {
        ucRetVal += 10 + (pucaData[1] - 'A');
    }
    else
    {
        ucRetVal += pucaData[1] - '0';
    }
    
    return(ucRetVal);
}

unsigned short int blurPPASCIIHexToShort(unsigned char *pucaData)
{
    unsigned short int uiRetVal;
    unsigned char ucaBuffer[2];
    
    //Convert first two digits (lower byte)
    ucaBuffer[0] = blurPPASCIIHexToByte(pucaData);
    //Convert second two digits (upper byte)
    ucaBuffer[1] = blurPPASCIIHexToByte(&pucaData[2]);
    
    uiRetVal = ucaBuffer[1];
    uiRetVal <<= 8;
    uiRetVal &= 0xFF00u;
    uiRetVal += ucaBuffer[0];
    
    return(uiRetVal);
}

unsigned char blurPPInputDataByte()
{
    unsigned char ucRetVal = 0;
    
    switch(ucBPPIncomingDataType)
    {
        case BLURPP_DATATYPE_ASCII:
            if(uiBPPIncomingDataEndIndex > (uiBPPIncomingDataPointerIndex + 1))
            {
                //Get the next byte from the ASCII input
                ucRetVal = blurPPASCIIHexToByte(&pucaBPPReceiveBuffer[uiBPPIncomingDataPointerIndex]);
                uiBPPIncomingDataPointerIndex += 2;    //Increment to next ASCII byte
            }
            break;
        case BLURPP_DATATYPE_BINARY:
            if(uiBPPIncomingDataEndIndex > uiBPPIncomingDataPointerIndex)
            {
                //See if byte is escape character to indicate need to transform byte
                if(CMD_ESCAPE == pucaBPPReceiveBuffer[uiBPPIncomingDataPointerIndex])
                {
                    //Escape character present, skip it
                    uiBPPIncomingDataPointerIndex++;
                    if(uiBPPIncomingDataEndIndex > uiBPPIncomingDataPointerIndex)
                    {
                        //Extract the byte from the input buffer
                        ucRetVal = pucaBPPReceiveBuffer[uiBPPIncomingDataPointerIndex];
                        //Transform because of escape
                        ucRetVal = ~(ucRetVal);
                        //Increment to next byte
                        uiBPPIncomingDataPointerIndex++;
                    }
                }
                else
                {
                    //Extract the byte from the input buffer
                    ucRetVal = pucaBPPReceiveBuffer[uiBPPIncomingDataPointerIndex];
                    //Increment to next byte
                    uiBPPIncomingDataPointerIndex++;
                }
            }
            break;
    }
    
    return(ucRetVal);
}

unsigned short int blurPPInputDataShort()
{
    unsigned short int uiRetVal = 0;
    
    switch(ucBPPIncomingDataType)
    {
        case BLURPP_DATATYPE_ASCII:
            if(uiBPPIncomingDataEndIndex > (uiBPPIncomingDataPointerIndex + 3))
            {
                //Extract the next short from the ASCII input
                uiRetVal = blurPPASCIIHexToShort(&pucaBPPReceiveBuffer[uiBPPIncomingDataPointerIndex]);
                //Increment pointer by 4 characters
                uiBPPIncomingDataPointerIndex += 4;
            }
            break;
        case BLURPP_DATATYPE_BINARY:
            {
                unsigned char caBuff[2];
                
                caBuff[1] = blurPPInputDataByte();  //Lower byte
                caBuff[0] = blurPPInputDataByte();  //Upper byte
                uiRetVal = caBuff[1];
                uiRetVal <<= 8;
                uiRetVal &= 0xFF00u;
                uiRetVal += caBuff[0];
            }
            break;
    }
    
    return(uiRetVal);
}

unsigned int blurPPInputDataInt()
{
    unsigned int uiRetVal = 0;
    unsigned short int uiaBuff[2];
    
    if((BLURPP_DATATYPE_ASCII == ucBPPIncomingDataType) || (BLURPP_DATATYPE_BINARY == ucBPPIncomingDataType))
    {     
        //Low word
        uiaBuff[1] = blurPPInputDataShort();
        //High word
        uiaBuff[0] = blurPPInputDataShort();
        //Put int together
        uiRetVal = uiaBuff[0];
        uiRetVal <<= 16;
        uiRetVal &= 0xFFFF0000u;
        uiRetVal += uiaBuff[1];
    }
    
    return(uiRetVal);
}

float blurPPInputDataFloat()
{
    float fRetVal = 0.0f;
    unsigned char caBuff[4];
    
    if((BLURPP_DATATYPE_ASCII == ucBPPIncomingDataType) || (BLURPP_DATATYPE_BINARY == ucBPPIncomingDataType))
    {
        caBuff[0] = blurPPInputDataByte();
        caBuff[1] = blurPPInputDataByte();
        caBuff[2] = blurPPInputDataByte();
        caBuff[3] = blurPPInputDataByte();
        memcpy(&fRetVal,caBuff,4);
    }
    
    return(fRetVal);
}

double blurPPInputDataDouble()
{
    double dRetVal = 0.0f;
    unsigned char caBuff[8];
    
    if((BLURPP_DATATYPE_ASCII == ucBPPIncomingDataType) || (BLURPP_DATATYPE_BINARY == ucBPPIncomingDataType))
    {
        caBuff[0] = blurPPInputDataByte();
        caBuff[1] = blurPPInputDataByte();
        caBuff[2] = blurPPInputDataByte();
        caBuff[3] = blurPPInputDataByte();
        caBuff[4] = blurPPInputDataByte();
        caBuff[5] = blurPPInputDataByte();
        caBuff[6] = blurPPInputDataByte();
        caBuff[7] = blurPPInputDataByte();
        memcpy(&dRetVal,caBuff,8);
    }
    
    return(dRetVal);
}

void blurPPInputDataByteArray(unsigned char *pucaData,unsigned int uiCount)
{
    if((BLURPP_DATATYPE_ASCII == ucBPPIncomingDataType) || (BLURPP_DATATYPE_BINARY == ucBPPIncomingDataType))
    {
        unsigned int uiLoop;
        
        for(uiLoop = 0;uiLoop < uiCount;uiLoop++)
        {
            *pucaData = blurPPInputDataByte();
            pucaData++;
        }
    }
}

void blurPPSetOutgoingSourceID(unsigned char ucID)
{
    ucBPPOutgoingSourceID = ucID;
}

void blurPPSetOutgoingDestinationID(unsigned char ucID)
{
    ucBPPOutgoingDestinationID = ucID;
}

void blurPPSetOutgoingSequenceNumber(unsigned short int uiSeq)
{
    uiBPPOutgoingSequenceNumber = uiSeq;
}

void blurPPByteToASCII(unsigned char ucData, unsigned char *pucaData)
{
    char caBuff[3];
    
    memset(caBuff,0,3);
    sprintf(caBuff,"%02X",ucData);
    pucaData[0] = caBuff[0];
    pucaData[1] = caBuff[1];
}

void blurPPShortToASCII(unsigned short int uiData, unsigned char *pucaData)
{
    char caBuff[5];
    
    memset(caBuff,0,5);
    sprintf(caBuff,"%04X",uiData);
    //Low word
    pucaData[0] = caBuff[2];
    pucaData[1] = caBuff[3];
    //Hi word
    pucaData[2] = caBuff[0];
    pucaData[3] = caBuff[1];
}

void blurPPBeginNewPacket(unsigned char ucCommandID, unsigned char ucDataType)
{
    //Set the start delimiter
    pucaBPPTransmitBuffer[0] = CMD_START;
    //Set the command ID
    blurPPByteToASCII(ucCommandID,&pucaBPPTransmitBuffer[1]);
    //Zero out the length for now since packet is being built
    memset(&pucaBPPTransmitBuffer[3],0,4);
    //Set the source ID
    blurPPByteToASCII(ucBPPOutgoingSourceID,&pucaBPPTransmitBuffer[7]);
    //Set the destination ID
    blurPPByteToASCII(ucBPPOutgoingDestinationID,&pucaBPPTransmitBuffer[9]);
    //Set the sequence number
    blurPPShortToASCII(uiBPPOutgoingSequenceNumber,&pucaBPPTransmitBuffer[11]);
    //Set the data type
    ucBPPOutgoingDataType = ucDataType;
    pucaBPPTransmitBuffer[15] = ucBPPOutgoingDataType;
    //Set data pointer to first data payload byte index
    uiBPPOutgoingDataPointerIndex = FIRST_DATA_BYTE_INDEX;
    //Reset
    uiBPPOutgoingTotalPacketSize = 0;
}

void blurPPNewErrorPacket(unsigned char cErrorData)
{
    pucaBPPTransmitBuffer[0] = CMD_NAK;
    pucaBPPTransmitBuffer[1] = cErrorData;
    pucaBPPTransmitBuffer[2] = CMD_TERM1;
    pucaBPPTransmitBuffer[3] = CMD_TERM2;
    uiBPPOutgoingTotalPacketSize = 4;
}

void blurPPTerminatePacket()
{
    unsigned char ucCheckSum;
    
    uiBPPOutgoingTotalPacketSize = (uiBPPOutgoingDataPointerIndex - FIRST_DATA_BYTE_INDEX) + MIN_PACKET_INPUT_SIZE;
    //Add in packet length
    blurPPShortToASCII(uiBPPOutgoingTotalPacketSize,&pucaBPPTransmitBuffer[3]);
    //Add in data termination
    pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = CMD_DATA_END;
    uiBPPOutgoingDataPointerIndex++;
    //Add in two zeroes for checksum to they won't interfere with the calc
    pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = 0;
    uiBPPOutgoingDataPointerIndex++;
    pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = 0;
    uiBPPOutgoingDataPointerIndex++;
    //Add the terminators
    pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = CMD_TERM1;
    uiBPPOutgoingDataPointerIndex++;
    pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = CMD_TERM2;
    uiBPPOutgoingDataPointerIndex++;
    //Calculate the checksum
    ucCheckSum = blurPPCalcChecksum(pucaBPPTransmitBuffer,uiBPPOutgoingTotalPacketSize);
    //Put the checksum into the packet
    blurPPByteToASCII(ucCheckSum,&pucaBPPTransmitBuffer[uiBPPOutgoingTotalPacketSize - 4]);
}

void blurPPOutputDataByte(unsigned char ucData)
{
    switch(ucBPPOutgoingDataType)
    {
        case BLURPP_DATATYPE_ASCII:
            if(uiBPPTransmitBufferSize > (uiBPPOutgoingDataPointerIndex + 1))
            {
                blurPPByteToASCII(ucData,&pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex]);
                uiBPPOutgoingDataPointerIndex += 2;
            }
            break;
        case BLURPP_DATATYPE_BINARY:
            {
                switch(ucData)
                {
                    case CMD_START:
                    case CMD_ESCAPE:
                    case CMD_DATA_END:
                    case CMD_TERM1:
                    case CMD_TERM2:
                    case CMD_NAK:
                        if(uiBPPTransmitBufferSize > (uiBPPOutgoingDataPointerIndex + 1))
                        {
                            pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = CMD_ESCAPE;
                            uiBPPOutgoingDataPointerIndex++;
                            pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = ~(ucData);
                            uiBPPOutgoingDataPointerIndex++;
                        }
                        break;
                    default:
                        if(uiBPPTransmitBufferSize > uiBPPOutgoingDataPointerIndex)
                        {
                            pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = ucData;
                            uiBPPOutgoingDataPointerIndex++;
                        }
                }
            }
            break;
    }
}

void blurPPOutputDataShort(unsigned short int uiData)
{
    if(BLURPP_DATATYPE_ASCII == ucBPPOutgoingDataType)
    {
        char caBuff[5];
        
        memset(caBuff,0,5);
        sprintf(caBuff,"%04X",uiData);
        pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = caBuff[2];
        uiBPPOutgoingDataPointerIndex++;
        pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = caBuff[3];
        uiBPPOutgoingDataPointerIndex++;
        pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = caBuff[0];
        uiBPPOutgoingDataPointerIndex++;
        pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = caBuff[1];
        uiBPPOutgoingDataPointerIndex++;
    }
    else if(BLURPP_DATATYPE_BINARY == ucBPPOutgoingDataType)
    {
        unsigned char caBuff[2];
        
        memcpy(caBuff,&uiData,2);
        blurPPOutputDataByte(caBuff[0]);
        blurPPOutputDataByte(caBuff[1]);
    }
}

void blurPPOutputDataInt(unsigned int uiData)
{
    unsigned char ucaData[4];
    
    memcpy(ucaData,&uiData,4);
    if(BLURPP_DATATYPE_ASCII == ucBPPOutgoingDataType)
    {
        char caBuffer[10];
        
        memset(caBuffer,0,10);
        sprintf(caBuffer,"%02X",(unsigned int)ucaData[3]);
        sprintf(&caBuffer[2],"%02X",(unsigned int)ucaData[2]);
        sprintf(&caBuffer[4],"%02X",(unsigned int)ucaData[1]);
        sprintf(&caBuffer[6],"%02X",(unsigned int)ucaData[0]);
        pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = caBuffer[6];
        uiBPPOutgoingDataPointerIndex++;
        pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = caBuffer[7];
        uiBPPOutgoingDataPointerIndex++;
        pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = caBuffer[4];
        uiBPPOutgoingDataPointerIndex++;
        pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = caBuffer[5];
        uiBPPOutgoingDataPointerIndex++;
        pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = caBuffer[2];
        uiBPPOutgoingDataPointerIndex++;
        pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = caBuffer[3];
        uiBPPOutgoingDataPointerIndex++;
        pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = caBuffer[0];
        uiBPPOutgoingDataPointerIndex++;
        pucaBPPTransmitBuffer[uiBPPOutgoingDataPointerIndex] = caBuffer[1];
        uiBPPOutgoingDataPointerIndex++;
    }
    else if(BLURPP_DATATYPE_BINARY == ucBPPOutgoingDataType)
    {
        blurPPOutputDataByte(ucaData[0]);
        blurPPOutputDataByte(ucaData[1]);
        blurPPOutputDataByte(ucaData[2]);
        blurPPOutputDataByte(ucaData[3]);
    }
}

void blurPPOutputDataFloat(float fData)
{
    unsigned char caBuff[4];
    
    memcpy(caBuff,&fData,4);
    blurPPOutputDataByte(caBuff[0]);
    blurPPOutputDataByte(caBuff[1]);
    blurPPOutputDataByte(caBuff[2]);
    blurPPOutputDataByte(caBuff[3]);
}

void blurPPOutputDataDouble(double dData)
{
    unsigned char caBuff[8];
    
    memcpy(caBuff,&dData,8);
    blurPPOutputDataByte(caBuff[0]);
    blurPPOutputDataByte(caBuff[1]);
    blurPPOutputDataByte(caBuff[2]);
    blurPPOutputDataByte(caBuff[3]);
    blurPPOutputDataByte(caBuff[4]);
    blurPPOutputDataByte(caBuff[5]);
    blurPPOutputDataByte(caBuff[6]);
    blurPPOutputDataByte(caBuff[7]);
}

void blurPPOutputDataByteArray(unsigned char *pucaData, unsigned int uiCount)
{
    unsigned int uiLoop;
    
    for(uiLoop = 0;uiLoop < uiCount;uiLoop++)
    {
        blurPPOutputDataByte(*pucaData);
        pucaData++;
    }
}

unsigned int blurPPOutgoingPacketSize()
{
    return(uiBPPOutgoingTotalPacketSize);
}

/* [] END OF FILE */
