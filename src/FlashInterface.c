/* ========================================
 *
 * Copyright Blur Product Development, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur Product Development.
 *
 * ========================================
*/

//Includes
#include "FlashInterface.h"
#include "nrf_delay.h"
//#include "nrfx_coredep.h"
#include "SPIHandler.h"

#define FLASH_CS_ON                     nrf_gpio_pin_clear(SPI_CS_PIN); NRFX_DELAY_US(15)
#define FLASH_CS_OFF                    NRFX_DELAY_US(15); nrf_gpio_pin_set(SPI_CS_PIN); NRFX_DELAY_US(15)

/* Get 8 bits of 16 bit value. */
#define LO8(x)                  ((uint8_t) ((x) & 0xFFu))
#define HI8(x)                  ((uint8_t) ((uint16_t)(x) >> 8))

// Macros/defines
#define JEDEC_RETRIES   0x03u

#define FLASH_BLOCK_SIZE        0x00010000u //WARNING: CHECK DATASHEET, 64kB? 

#define FLASH_CMD_WRITE_STS_1   0x01u
#define FLASH_CMD_WRITE         0x02u
#define FLASH_CMD_READ          0x03u
#define FLASH_CMD_READ_STS_1    0x05u
#define FLASH_CMD_WRITE_ENABLE  0x06u
#define FLASH_CMD_WRITE_ENABLEV 0x50u
#define FLASH_CMD_READ_JEDEC    0x9Fu
#define FLASH_CMD_ERS_SECTOR    0x20u
#define FLASH_CMD_ERS_64_BLK    0xD8u
#define FLASH_CMD_DEEP_PD       0xB9u
#define FLASH_CMD_DEVICEID      0xABu

#define FLASH_STATUS1_BUSY      0x01u

//Global variables
uint8_t ucaFlashJEDECID_[3];

//Local variables
uint32_t uiFlashAddress;
uint8_t ucFlashAddressHIB;
uint8_t ucFlashAddressMIB;
uint8_t ucFlashAddressLOB;
uint8_t ucFlashStatus1;
uint8_t ucFlashLoop;
uint16_t uiFlashLoop;
uint32_t uiFlashDisplayDataCount;
uint32_t uiNumberOfSteps;
bool bChipErased = false;

//Local functions
void flashAddressToBytes();
bool flashIsBusy();
void flashWriteByte(uint8_t ucData);
void flashWriteBuffer(uint8_t *pucaData, uint16_t ucCount);
void flashWritePage(uint8_t *pucaData, uint16_t ucCount);

void flashReadPage(uint8_t *pucaData, uint16_t uiCount);

bool FlashIsPresent()
{
    bool bOK = false;
    uint8_t ucRetries = 0;
    
    do
    {
        //SPIReset();
        FLASH_CS_ON;
        SPIWriteByte(FLASH_CMD_READ_JEDEC);
        SPIReadBufferFast(ucaFlashJEDECID_,3);
        FLASH_CS_OFF;
        if((0x00u != ucaFlashJEDECID_[0]) && (0x00u != ucaFlashJEDECID_[1]) && (0x00u != ucaFlashJEDECID_[2]))
        {
            if((0xFFu != ucaFlashJEDECID_[0]) && (0xFFu != ucaFlashJEDECID_[1]) && (0xFFu != ucaFlashJEDECID_[2]))
            {
                bOK = true;
            }
        }
    }
    while((false == bOK) && (JEDEC_RETRIES > ucRetries++));
    
    return(bOK);
}

/* ************** FLASH SECTOR-LEVEL FUNCTIONS ************** */
// Sectors are 4096 bytes, e.g. sector 0 : 000000h .. 00FFFh
// NOTE: Sectors can vary in size between different flash P/Ns, it is critical that this be checked.
void flashReadSector(uint32_t sector_start_addr, uint8_t * pBuf)
{
    if(0u == (sector_start_addr % FLASH_SECTOR_SIZE))
    {
        uiFlashAddress = sector_start_addr;
        flashAddressToBytes();
        flashReadReadGeneric((uint8_t*)pBuf, FLASH_SECTOR_SIZE);
    }
}

void flashWriteSector(uint32_t sector_start_addr, uint8_t * pBuf)
{
    if(0u == (sector_start_addr % FLASH_SECTOR_SIZE))
    {
        uiFlashAddress = sector_start_addr;
        flashAddressToBytes(); 
        flashWriteBuffer((uint8_t*)pBuf,FLASH_SECTOR_SIZE);
    }
}

void flashEraseSector(uint32_t sector_start_addr)
{
    // check that start_addr is in range
    
    // check that sector_start_addr lines up with the sector boundaries
    if(0u == (sector_start_addr % FLASH_SECTOR_SIZE))
    {
        uiFlashAddress = sector_start_addr;
        flashAddressToBytes();
        
        flashAddressToBytes();
        FLASH_CS_ON;
        //SPIReset();
        SPIWriteByte(FLASH_CMD_WRITE_ENABLEV);
        FLASH_CS_OFF;
        FLASH_CS_ON;
        //SPIReset();
        SPIWriteByte(FLASH_CMD_WRITE_STS_1);
        SPIWriteByte(0x00); //Write 0 to enable writes
        FLASH_CS_OFF;
        FLASH_CS_ON;
        //SPIReset();
        SPIWriteByte(FLASH_CMD_WRITE_ENABLE);
        FLASH_CS_OFF;
        FLASH_CS_ON;
        //SPIReset();
        SPIWriteByte(FLASH_CMD_READ_STS_1);
        ucFlashStatus1 = SPIReadByte();
        FLASH_CS_OFF;
        FLASH_CS_ON;
        //SPIReset();
        SPIWriteByte(FLASH_CMD_ERS_SECTOR);
        SPIWriteByte(ucFlashAddressHIB);
        SPIWriteByte(ucFlashAddressMIB);
        SPIWriteByte(ucFlashAddressLOB);
        FLASH_CS_OFF;
        NRFX_DELAY_US(1000);
        FLASH_CS_ON;
        while(true == flashIsBusy())
        {
            NRFX_DELAY_US(1000);
            //CyBle_ProcessEvents();
        }
        FLASH_CS_OFF;
    }else{
        NRF_LOG_INFO("Error: Erase sector unaligned\r\n");
    }
    NRF_LOG_FLUSH();
}


void flashEraseTimestepBlock()
{
    uint32_t uiFlashAddressEnd = (flashScanForLastTimestep() * TIMESTEP_OUTPUT_SIZE);
    uiFlashAddressEnd += TIMESTEP_ADDRESS_START;
    
    for(uiFlashAddress = TIMESTEP_ADDRESS_START;uiFlashAddress < uiFlashAddressEnd;uiFlashAddress += FLASH_BLOCK_SIZE)
    {
        flashAddressToBytes();
        FLASH_CS_ON;
        //SPIReset();
        SPIWriteByte(FLASH_CMD_WRITE_ENABLEV);
        FLASH_CS_OFF;
        FLASH_CS_ON;
        //SPIReset();
        SPIWriteByte(FLASH_CMD_WRITE_STS_1);
        SPIWriteByte(0x00); //Write 0 to enable writes
        FLASH_CS_OFF;
        FLASH_CS_ON;
        //SPIReset();
        SPIWriteByte(FLASH_CMD_WRITE_ENABLE);
        FLASH_CS_OFF;
        FLASH_CS_ON;
        //SPIReset();
        SPIWriteByte(FLASH_CMD_READ_STS_1);
        ucFlashStatus1 = SPIReadByte();
        FLASH_CS_OFF;
        FLASH_CS_ON;
        //SPIReset();
        SPIWriteByte(FLASH_CMD_ERS_64_BLK);
        SPIWriteByte(ucFlashAddressHIB);
        SPIWriteByte(ucFlashAddressMIB);
        SPIWriteByte(ucFlashAddressLOB);
        FLASH_CS_OFF;
        NRFX_DELAY_US(1000);
        FLASH_CS_ON;
        while(true == flashIsBusy())
        {
            NRFX_DELAY_US(1000);
            //CyBle_ProcessEvents();
            //kickWatchdog();
        }
        FLASH_CS_OFF;
    }
}

/* ************** FLASH R/W FUNCTIONS ************** */

void flashWriteTimestepData(uint32_t uiIndex,Timestep_t * pInfo)
{
    uiFlashAddress = (uiIndex * TIMESTEP_OUTPUT_SIZE);
    uiFlashAddress += TIMESTEP_ADDRESS_START;
    flashAddressToBytes();
    flashWriteBuffer((uint8_t*)pInfo,sizeof(Timestep_t));
}

void flashReadTimestepData(uint32_t uiIndex,Timestep_t * pInfo)
{
    uiFlashAddress = (uiIndex * TIMESTEP_OUTPUT_SIZE);
    uiFlashAddress += TIMESTEP_ADDRESS_START;
    flashAddressToBytes();
    flashReadReadGeneric((uint8_t*)pInfo,sizeof(Timestep_t));
}

uint32_t flashScanForLastTimestep()
{
    uint32_t uiIndex = 0;
    Timestep_t sDummyPacket;
    flashReadTimestepData(uiIndex, &sDummyPacket);
    if(0xFFFFFFFFu == sDummyPacket.uiTimeStamp) { return(0);}
    while(0xFFFFFFFFu != sDummyPacket.uiTimeStamp)
    {
        flashReadTimestepData(uiIndex++, &sDummyPacket);
        if(uiIndex > NUM_TIMESTEP_SLOTS)
        {
            return(0);
        }
    }
    if(uiIndex > 0)
    {
        return(uiIndex - 1);
    }else{
        return(uiIndex);
    }
}


/* ************** FLASH INTERNAL FUNCTIONS ************** */

bool is_struct_empty(uint8_t * sPtr, uint16_t len)
{
    bool is_empty = true;
    for(int i = 0; i < len; i++)
    {
        is_empty &= (sPtr[i] == 0xFFu);    
    }
    return(is_empty);
}

bool flashChipErase()
{   
    bool bOK = false;
    int iLoop;
    for(iLoop = 0; iLoop < FLASH_NUM_SECTORS; iLoop++)
    {
        flashEraseSector(iLoop * FLASH_SECTOR_SIZE);
        if( 0 == (iLoop % 20)) NRF_LOG_INFO("Erasing: %4d/%4d\r\n",iLoop, FLASH_NUM_SECTORS);
    }
    NRF_LOG_INFO("Success, erased %4d/%4d sectors\r\n",iLoop+1, FLASH_NUM_SECTORS);

    BLUR_SET_BIT(ucGlobalFlags1_, eGlobalFlag1_ChipErased);

    bOK = true;
    return(bOK);
}

void flashTerminateOperation()
{
    FLASH_CS_OFF;
}

void flashWriteByte(uint8_t ucData)
{
    flashAddressToBytes();
    FLASH_CS_ON;
    SPIWriteByte(FLASH_CMD_WRITE_ENABLE);
    FLASH_CS_OFF;
    FLASH_CS_ON;
    SPIWriteByte(FLASH_CMD_WRITE);
    SPIWriteByte(ucFlashAddressHIB);
    SPIWriteByte(ucFlashAddressMIB);
    SPIWriteByte(ucFlashAddressLOB);
    SPIWriteByte(ucData);
    FLASH_CS_OFF;
    FLASH_CS_ON;
    while(true == flashIsBusy())
    {
        NRFX_DELAY_US(1);
    }
    FLASH_CS_OFF;
    uiFlashAddress++;
}

void flashWritePage(uint8_t *pucaData, uint16_t ucCount)
{
    if(ucCount != 256u) return;
    // TODO - check that address aligns on page boundary
    
    flashAddressToBytes();
    FLASH_CS_ON;
    SPIWriteByte(FLASH_CMD_WRITE_ENABLE);
    FLASH_CS_OFF;
    FLASH_CS_ON;
    SPIWriteByte(FLASH_CMD_WRITE);
    SPIWriteByte(ucFlashAddressHIB);
    SPIWriteByte(ucFlashAddressMIB);
    SPIWriteByte(ucFlashAddressLOB);
    
    SPIWriteBufferFast(&pucaData[0], 128);
    SPIWriteBufferFast(&pucaData[128], 128);
        
    FLASH_CS_OFF;
    FLASH_CS_ON;
    while(true == flashIsBusy())
    {
        NRFX_DELAY_US(1);
    }
    FLASH_CS_OFF;
    uiFlashAddress += 256;
}

void flashWriteBuffer(uint8_t *pucaData, uint16_t ucCount)
{
    uint16_t ucLocalLoop;
    
    for(ucLocalLoop = 0;ucLocalLoop < ucCount;ucLocalLoop++)
    {
        flashWriteByte(*pucaData);
        pucaData++;
    }
}

void flashAddressToBytes()
{
    ucFlashAddressHIB = uiFlashAddress >> 16;
    ucFlashAddressMIB = uiFlashAddress >> 8;
    ucFlashAddressLOB = LO8(uiFlashAddress);
}

bool flashIsBusy()
{
    bool bRetVal = false;
    
    //SPIReset();
    SPIWriteByte(FLASH_CMD_READ_STS_1);
    ucFlashStatus1 = SPIReadByte();
    if(FLASH_STATUS1_BUSY & ucFlashStatus1)
    {
        bRetVal = true;
    }
    
    return(bRetVal);
}

void flashReadReadGeneric(uint8_t *pucaData,uint16_t uiCount)
{
    flashAddressToBytes();
    FLASH_CS_ON;
    SPIWriteByte(FLASH_CMD_READ);
    SPIWriteByte(ucFlashAddressHIB);
    SPIWriteByte(ucFlashAddressMIB);
    SPIWriteByte(ucFlashAddressLOB);
    
    SPIReadBuffer(pucaData,uiCount);
    FLASH_CS_OFF;
    uiFlashAddress += uiCount;
}

void flashReadPage(uint8_t *pucaData, uint16_t uiCount)
{
    if(uiCount != 256u) return;
    
    flashAddressToBytes();
    FLASH_CS_ON;
    SPIWriteByte(FLASH_CMD_READ);
    SPIWriteByte(ucFlashAddressHIB);
    SPIWriteByte(ucFlashAddressMIB);
    SPIWriteByte(ucFlashAddressLOB);
    SPIReadBufferFast(&pucaData[0],128);
    SPIReadBufferFast(&pucaData[128],128);
    FLASH_CS_OFF;
    uiFlashAddress += uiCount;
}

// WARNING - additional logic will be required for unlock of device
void flashEnterDeepPowerDown()
{
    // Reduces power consumption from ~9-50uA to ~0.007-0.35uA
    FLASH_CS_ON;
    SPIWriteByte(FLASH_CMD_DEEP_PD);
    FLASH_CS_OFF;
    NRFX_DELAY_US(40); // From MX25 datasheet, don't pulse CS low again for 30 us
}

void flashExitDeepPowerDown()
{
    uint8_t dummy[3];
    FLASH_CS_ON;
    NRFX_DELAY_US(1);
    SPIWriteByte(FLASH_CMD_DEVICEID);
    SPIReadBufferFast(dummy,3);
    FLASH_CS_OFF;
    NRFX_DELAY_US(35);
}

bool flashCompareTimestepData(Timestep_t *pDataWritten, Timestep_t *pDataRead)
{
    bool bOK = false;

    if(pDataWritten->uiTimeStamp == pDataRead->uiTimeStamp)
    {
        bOK = true;
    }
    if(pDataWritten->uiSequenceNum == pDataRead->uiSequenceNum)
    {
        bOK &= true;
    }
    if(pDataWritten->ucTimeStepFlags == pDataRead->ucTimeStepFlags)
    {
        bOK &= true;
    }
    for(int i = 0; i < FIFO_SIZE; i++)
    {
        if(pDataWritten->uiPressure[i] == pDataRead->uiPressure[i])
        {
            bOK &= true;
        } 
    }
    if(pDataWritten->iVbatt == pDataRead->iVbatt)
    {
        bOK &= true;
    }
    if(pDataWritten->ucChecksum == pDataRead->ucChecksum)
    {
        bOK &= true;
    }

    return(bOK);

}


void flashUpdateNumSteps(uint32_t num_timesteps)
{
    uiNumberOfSteps = num_timesteps;
}

uint32_t flashGetLocalSteps()
{
    return(uiNumberOfSteps);
}

/* [] END OF FILE */
