##############
Error Handling
##############

.. _architecture-error-handling:

The Uromonitor enumerates and handles the following errors:

.. code-block:: c

    enum eSystemErrorCodes
    {
        eSystemErrorCode_None = 0,
        eSystemErrorCode_BLE,
        eSystemErrorCode_Flash,
        eSystemErrorCode_Pressure,
        eSystemErrorCode_ADC,
        eSystemErrorCode_RTC
    };

Each error has it's own set of recovery attempt parameters, and some can prevent the Uromonitor from further operation if detected.

.. list-table:: 
   :widths: 25 25 25 25 
   :header-rows: 1

   * - Name
     - Recovery
     - Reporting
     - Description
   * - eSystemErrorCode_BLE
     - Soft Reset
     - LED, UART, Flash
     - BLE stack error. Recoverable via system reset.
   * - eSystemErrorCode_Flash
     - 
     - LED, UART, BLE
     - Failure to communicate with flash interface.
   * - eSystemErrorCode_Pressure
     - 
     - LED, UART, Flash, BLE
     - Failure to communicate with pressure transducer. 
   * - eSystemErrorCode_ADC
     - 
     - LED, UART, Flash, BLE
     - Analog conversion issue.
   * - eSystemErrorCode_RTC
     - 
     - LED, UART, Flash, BLE
     - Real-time clock issue.