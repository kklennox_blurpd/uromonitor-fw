###
BLE
###

.. |uromonitor-service| replace:: 88ED0000-722D-0055-02DF-A2340946D9C0
.. |status-characteristic| replace:: 88ED0001-722D-0055-02DF-A2340946D9C0
.. |bulk-data-characteristic| replace:: 88ED0002-722D-0055-02DF-A2340946D9C0
.. |opcommand-characteristic| replace:: 88ED0003-722D-0055-02DF-A2340946D9C0

********
Services
********

The UroMonitor BLE Profile supports six standard services and one custom service.  
All data is formatted in little endian unless otherwise noted.

.. list-table:: 
   :widths: 25 25
   :header-rows: 1

   * - Name
     - UUID
   * - Generic Access
     - ``1800``
   * - Generic Attribute
     - ``1801``
   * - Device Information
     - ``180A``    
   * - Battery
     - ``180F``
   * - Bond Management
     - ``181E``
   * - :ref:`Uromonitor <uromonitor-characteristics>`
     - ``88ED0000-722D-0055-02DF-A2340946D9C0``

.. _uromonitor-characteristics:

**************************
UroMonitor Characteristics
**************************

.. list-table:: 
   :widths: 15 50 15 15 
   :header-rows: 1

   * - Characteristic Name
     - UUID
     - Properties
     - Permissions
   * - :ref:`Status <uromonitor-characteristic-status>`
     - ``88ED0001-722D-0055-02DF-A2340946D9C0``
     - Notify
     - Encrypted
   * - :ref:`Bulk Data <uromonitor-characteristic-bulk-data>`
     - ``88ED0002-722D-0055-02DF-A2340946D9C0``
     - Notify
     - Encrypted
   * - :ref:`Op Command <uromonitor-characteristic-opcommand>`
     - ``88ED0003-722D-0055-02DF-A2340946D9C0``
     - Read, Write, Notify
     - Encrypted

.. toctree::
    :maxdepth: 1
    :hidden:

    architecture-ble-status
    architecture-ble-bulk-data
    architecture-ble-opcommand

.. _advertising:

***********
Advertising
***********

Currently, the Uromonitor advertises the ``<<128-bit UUID>> (Uromonitor Device)`` in the advertisement packet. 

The Scan Response packet contains the ``<<Local Name>> (Uromonitor-xyz)`` 
and 8 bytes of ``<<Manufacturer Specific Data>>``. 
What this data represents is not specified yet and is a placeholder. 
A serial number seems logical to place here.

Different ``<<Manufacturer Specific Data>>`` could be added to the advertisement packet as well. 
The current scheme of splitting the local device name out into the scan response packet is 
due to length restrictions of the advertisement packet. 
Placing the mfg-specific data in the scan response vs. 
the advertisement packet confers a slight power savings during advertisement mode.

*******
Bonding
*******

Device bonding will be performed with the BLE Passkey workflow using Security Mode 1 Level 3.