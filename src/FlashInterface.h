/* ========================================
 *
 * Copyright Blur Product Development, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur Product Development.
 *
 * ========================================
*/

#ifndef FLASHINTERFACE_H
#define FLASHINTERFACE_H
#include "globals.h"
#include "SPIHandler.h"
#include "nrf_drv_gpiote.h"
#include "nrfx_gpiote.h"
#include "nordic_common.h"
#include "app_util_platform.h"
#include "project.h"
#include "nrf_delay.h"

// Defines
// Implementation specific defines for MX25R6435FZAIH0 
// Datasheet https://www.macronix.com/Lists/Datasheet/Attachments/7913/MX25R6435F,%20Wide%20Range,%2064Mb,%20v1.5.pdf  
#define FLASH_NUM_BLOCKS        (256u)
#define FLASH_NUM_SECTORS       (2048u)
#define FLASH_SECTOR_SIZE       0x00001000u // 4kB
#define FLASH_BLOCK_SIZE        0x00010000u //64kB
    
#define TIMESTEP_ADDRESS_START  (0u)
#define TIMESTEP_SECTORS        (FLASH_NUM_SECTORS) 
#define NUM_TIMESTEP_SLOTS      ((TIMESTEP_SECTORS*FLASH_SECTOR_SIZE)/sizeof(Timestep_t))
    
// Global variables
extern uint8_t ucaFlashJEDECID_[3];
    
extern bool FlashIsPresent();

extern void flashEraseTimestepBlock();

extern void flashWriteTimestepData(uint32_t uiIndex,Timestep_t * pInfo);
extern void flashReadTimestepData(uint32_t uiIndex,Timestep_t * pInfo);
extern bool flashCompareTimestepData(Timestep_t *pDataWritten, Timestep_t *pDataRead);
extern uint32_t flashScanForLastTimestep(void);

extern void flashUpdateNumSteps(uint32_t num_timesteps);
extern uint32_t flashGetLocalSteps();

void flashReadFirmwareData(uint32_t dataAddr, uint32_t dataSize, uint8_t *data);
void flashWriteFirmwareData(uint32_t dataAddr, uint32_t dataSize, uint8_t *data);
void flashClearFirmwareSlot();

extern void flashReadReadGeneric(uint8_t*,uint16_t uiCounts);

extern bool is_struct_empty(uint8_t * sPtr, uint16_t len);

extern bool flashChipErase();
extern void flashTerminateOperation();

extern void flashEnterDeepPowerDown();
extern void flashExitDeepPowerDown();

#endif

/* [] END OF FILE */
