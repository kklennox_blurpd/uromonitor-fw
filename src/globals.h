/* ========================================
 *
 * Copyright BlurPD, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur PD 2022
 *
 * ========================================
*/

#ifndef GLOBALS_H
#define GLOBALS_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "app_error.h"
#include "nrf_delay.h"
#include "nrf.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#define SW_REV_MAJOR    0x01u
#define SW_REV_MINOR    0x00u
#define SW_REV_BUILD    0x00u

#define TIMESTEP_OUTPUT_SIZE      (sizeof(Timestep_t))

#define CHECK_BIT(a,b)                  (a & b)
#define BLUR_SET_BIT(a,b)               (a |= b)
#define RESET_BIT(a,b)                  (a &= ~(b))

#define FIFO_SIZE      30

#define LOG(s)         NRF_LOG_INFO(#s); NRF_LOG_FLUSH()
#define LOG_NUM(x)     NRF_LOG_INFO(x); NRF_LOG_FLUSH()

//Status Struct - New parameters can be added as needed
#pragma pack(1) //byte aligned
typedef struct 
{   
    uint32_t uiTimeStamp;
    uint16_t uiSequenceNum;
    uint8_t ucTimeStepFlags;
    uint32_t uiPressure[30];
    int16_t iVbatt;
    uint8_t ucChecksum;
} Timestep_t, *p_Timestep_t;
#pragma pack(0)

enum eGlobalFlags1
{
    eGlobalFlag1_PressureFifoFull   =        0x01,
    eGlobalFlag1_DataReady          =        0x02,
    eGlobalFlag1_ChipErased         =        0x04,
    eGlobalFlag1_FifoReadError      =        0x08,
    eGlobalFlag1_TimerTick          =        0x10
};

enum eTimeStepFlags
{
    eTimeStepFlag_Sensor_Reading    =       0x01, 
    eTimeStepFlag_Log               =       0x02
};

extern volatile uint8_t ucGlobalFlags1_;
#endif
/* [] END OF FILE */