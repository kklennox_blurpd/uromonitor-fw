#########################
Status Characteristic
#########################

.. |packet-size| replace:: 130

.. contents::

.. _uromonitor-characteristic-status:

The Status characteristic provides the central device with 
device state information.

.. list-table:: 
   :widths: 50 25 25
   :header-rows: 1

   * - UUID
     - Properties
     - Permissions
   * - ``88ED0001-722D-0055-02DF-A2340946D9C0``
     - Notify
     - Encrypted

***********
Data Format
***********

The status characteristic is represented by a struct in the firmware:

.. list-table:: 
   :widths: 25 25 25 50
   :header-rows: 1

   * - Byte Position
     - Format
     - Parameter
     - Description
   * - 1
     - uint32
     - Device Status
     - Bit field indicating device status
   * - 5
     - uint32
     - Device Error
     - Bit field indicating device errors
   * - 9
     - uint32
     - Timestep Count
     - Number of chunks of data
   * - 13
     - uint32
     - Power On Time
     - Seconds device has been powered on
   * - 17
     - uint8[|packet-size|]
     - :ref:`Timestep <packet-format>`
     - Most recent Timestep data, see :ref:`data format <packet-format>`
   * - 17 + |packet-size|
     - uint16
     - E2E-Checksum
     - 16 bit checksum calculated over the entire packet

************
Status Field
************

The status bitfield is as follows:

.. list-table:: 
   :widths: 25 25 25 50
   :header-rows: 1

   * - Bit Position
     - Format
     - Parameter
     - Description
   * - 1
     - bit
     - 
     - 
   * - 2
     - bit
     - 
     - 
   * - 3
     - bit
     - 
     - 
   * - 4
     - bit
     - 
     - 
   * - 5
     - bit
     - 
     - 
   * - 6
     - bit
     - 
     - 
   * - 7
     - bit
     - 
     - 
   * - 8
     - bit
     - 
     - 

************
Error Field
************

The error bitfield is as follows:

.. list-table:: 
   :widths: 25 25 25 50
   :header-rows: 1

   * - Bit Position
     - Format
     - Parameter
     - Description
   * - 1
     - bit
     - 
     - 
   * - 2
     - bit
     - 
     - 
   * - 3
     - bit
     - 
     - 
   * - 4
     - bit
     - 
     - 
   * - 5
     - bit
     - 
     - 
   * - 6
     - bit
     - 
     - 
   * - 7
     - bit
     - 
     - 
   * - 8
     - bit
     - 
     - 