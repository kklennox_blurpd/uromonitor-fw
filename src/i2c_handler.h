/* ========================================
 *
 * Copyright Blur Product Development, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur Product Development.
 *
 * ========================================
*/
/** @file
 *
 * @brief Functions for initializing and handling the I2C bus.
 */

#ifndef I2C_HANDLER_H__
#define I2C_HANDLER_H__

#include <stdint.h>
#include <stdbool.h>

extern void i2c_init();
extern void i2c_scan();
extern bool i2c_write_reg_byte(uint8_t ucAddress, uint8_t ucRegister, uint8_t ucByte);
extern bool i2c_read_register(uint8_t ucAddress, uint8_t ucRegister, uint8_t * pDest, uint8_t ucBytesToRead);

#endif // I2C_HANDLER_H__
