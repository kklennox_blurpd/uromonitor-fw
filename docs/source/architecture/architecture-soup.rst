########
SOUP/OTS
########

******************************
Software of Unknown Provenance
******************************

.. _soup:

.. list-table:: 
   :widths: 15 15 15 15 15 15 15 
   :header-rows: 1

   * - Component
     - Software Name
     - Version
     - URL
     - Description
     - Manufacturer
     - License
   * - Unit/Interaction Test
     - Unity Test
     - 
     - `unity <https://github.com/ThrowTheSwitch/Unity>`_
     - Unit test framework for embedded C
     - `ThrowTheSwitch <http://www.throwtheswitch.org/>`_
     - `MIT <https://github.com/ThrowTheSwitch/Unity/blob/master/LICENSE.txt>`__
   * - Unit/Interaction Test
     - CMock
     - 
     - `cmock <https://github.com/ThrowTheSwitch/CMock>`_
     - Test mocking framework for embedded C
     - ThrowTheSwitch_
     - `MIT <https://github.com/ThrowTheSwitch/CMock/blob/master/LICENSE.txt>`__
   * - Unit/Interaction Test
     - Fake Function Framework
     - 
     - `fake_function_framework <https://github.com/ElectronVector/fake_function_framework>`_
     - Test function mocking framework for embedded C
     - `ElectronVector <http://www.electronvector.com/>`__
     - `MIT <https://github.com/ElectronVector/fake_function_framework/blob/master/LICENSE>`__

*****************
Development Tools
*****************

.. _development-tools:

.. list-table::
   :widths: 15 15 15 15 15 15 15 
   :header-rows: 1

   * - Component
     - Software Name
     - Version
     - URL
     - Description
     - Manufacturer
     - License
   * - Firmware
     - Segger Embedded Studio
     - 6.20a
     - `Segger Embedded Studio <https://www.segger.com/products/development-tools/embedded-studio/>`__
     - IDE for Nordic devices
     - Segger
     - 
   * - GitLab Repo
     - 
     - 
     - 
     - 
     - 
     - 
   * - Documentation
     - Sphinx
     - 
     - 
     - 
     - 
     - 
   * - Documentation
     - Breathe
     - 
     - 
     - 
     - 
     - 
   * - Documentation
     - Doxygen
     - 
     - 
     - 
     - 
     - 
   * - Unit/Interaction Test
     - Ceedling
     - 
     - `ceedling <https://github.com/ThrowTheSwitch/Ceedling>`_
     - 
     - ThrowTheSwitch_
     - `LICENSE <https://github.com/ThrowTheSwitch/Ceedling/blob/master/license.txt>`__

*********************
Development Languages
*********************

.. _development-languages:

.. list-table::
   :widths: 15 15 15
   :header-rows: 1

   * - Component
     - Language
     - Version
   * - Firmware
     - C
     - 
   * - Documentation
     - reStructured Text
     - 

****************
Acknowledgements
****************

Unity
=====

This product includes software developed for the Unity Project, 
by Mike Karlesky, Mark VanderVoord, and Greg Williams 
and other contributors

