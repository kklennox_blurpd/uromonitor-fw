############
Unit Testing
############

.. _setup-unit-testing:

This project features unit testing with `Unity`__. 
Unity is designed specifically for small embedded systems, 
and provides a powerful framework for easily running unit tests on a modular basis.

To make test setup easier, more automated, and more convenient, 
the `Ceedling`__ build system provides an all-in-one, 
locally installed package for Unity, 
and it's helper projects `CMock`__ and `Fake Function Framework`__. 

 __ http://www.throwtheswitch.org/unity
 __ http://www.throwtheswitch.org/ceedling
 __ http://www.throwtheswitch.org/cmock
 __ https://github.com/meekrosoft/fff

 
**********
Installing
**********

Ruby
==========

Ceedling requires Ruby and GCC. To start, get the `Windows RubyInstaller <https://rubyinstaller.org/downloads/>`_:

.. warning::
   
   The 3.1.1-1 version of Ruby is not compatible with the locally installed Ceedling version in the project as of 4/4/22. 
   
   Make sure to download and install  ``Ruby+Devkit 3.0.3-1 (x64)``, 
   and be sure to allow ``Add Ruby executables to your PATH`` when prompted.

After installation is complete, confirm that the installation is successful 
by running the following command in either cmd or Powershell:

.. code-block:: doscon

   $ ruby --version
 
Ceedling
========

Ceedling has already been deployed locally to the project, but we need to still install Ceedling on our machine:

.. code-block:: doscon

   $ gem install ceedling

In a cmd or Powershell session, navigate to the ``test-ceedling/`` directory and run the following command:

.. code-block:: doscon

   $ ceedling version

If the above command runs, you're all set. Time to start running tests!

.. note:: 
   
   In the future, if an update to any of Ceedling, Unity, 
   or CMock is required, you can run 
   
   .. code-block:: doscon

      $ ceedling upgrade --local test-ceedling

   Be aware that this can modify the ``project.yml`` config. 
   To prevent this, either back up the project file before updating, 
   or run the command 
   
   .. code-block:: doscon

      $ ceedling upgrade --local --no_configs test-ceedling`

*********************
Running Tests Locally
*********************

Using Powershell or cmd, navigate to the test-ceedling/ directory.

.. code-block:: doscon

   $ ceedling test:all

**********************
Running Tests Remotely
**********************

Remote testing is not yet implemented.

************************ 
Running Coverage Reports
************************

Using Powershell or cmd, navigate to the test-ceedling directory. Report is available in archive/

.. code-block:: doscon

   $ ceedling gcov:all utils:gcov

***************
Reference Guide
***************

