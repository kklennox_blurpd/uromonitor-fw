/* ========================================
 *
 * Copyright Blur Product Development, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur Product Development.
 *
 * ========================================
*/
/** @file
 *
 * @brief Functions for initializing and handling BLE events,
 *        and for passing data to the SoftDevice stack.
 */

#ifndef BLE_HANDLER_H__
#define BLE_HANDLER_H__

extern void ble_stack_init();
extern void gap_params_init();
extern void gatt_init();
extern void services_init();
extern void conn_params_init();

extern void advertising_init();
extern void advertising_start(void);

#endif // BLE_HANDLER_H__
