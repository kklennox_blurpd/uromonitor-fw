/* ========================================
 *
 * Copyright Blur Product Development, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur Product Development.
 *
 * ========================================
*/

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "gpio_handler.h"
#include "project.h"

#include "nrf.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_drv_gpiote.h" // app_button.c uses the old driver, so we should too. 
#include "nrfx_gpiote.h"
#include "app_button.h"     // Nordic-provided button debouncing
#include "app_timer.h"

// Status pins
#define HI_ACCURACY_DISABLED                              (false)
static const nrfx_gpiote_in_config_t m_gpiote_input_cfg   = {                                                   \
                                                                .sense = NRF_GPIOTE_POLARITY_TOGGLE,            \
                                                                .pull = NRF_GPIO_PIN_PULLUP,                    \
                                                                .is_watcher = false,                            \
                                                                .hi_accuracy = HI_ACCURACY_DISABLED,            \
                                                                .skip_gpio_setup = false,                       \
                                                            };

// Forward declare status pins interrupt handler
void gpio_evt_handler(nrfx_gpiote_pin_t pin, nrf_gpiote_polarity_t action);


// Nordic app_button handling, for debouncing and event callbacks
#define BUTTON_DEBOUNCE_TIME_MS                (100u)
#define NUM_BUTTONS                            (1u)

// Forward declare button interrupt handler
void button_evt_handler(uint8_t ucPinNum, uint8_t ucButtonAction);

static const app_button_cfg_t m_button_cfg  = {                                                                 \
                                                                    .pin_no           = BUTTON_PIN,              \
                                                                    .active_state     = APP_BUTTON_ACTIVE_HIGH,  \
                                                                    .pull_cfg         = NRF_GPIO_PIN_NOPULL,    \
                                                                    .button_handler   = button_evt_handler,     \
                                                 };

static const app_button_cfg_t m_buttons[NUM_BUTTONS] = { m_button_cfg };

#define GPIO_TIMER_LONG_PRESS_TIME                (2000u)
APP_TIMER_DEF(m_shutdown_timer);

void shutdown_timer_callback()
{

    NRF_LOG_WARNING("Button Event: Long Press");
    
    NRF_LOG_WARNING("System Powering Off");
    NRF_LOG_FLUSH();

    pwr_ctrl_off();

    while(1)
    {
    }
    //ret_code_t ret_code;
    //ret_code = sd_power_gpregret_clr(GPREGRET_REG, 0xFF);
    //APP_ERROR_CHECK(ret_code);
    //ret_code = sd_power_gpregret_set(GPREGRET_REG, RESET_REASON_APP_REQ_SHUTDOWN);
    //APP_ERROR_CHECK(ret_code);
    //nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_RESET);
}

void gpio_handler_init(void)
{
  nrfx_err_t err_code;

  // Button setup
  err_code = app_button_init(m_buttons, NUM_BUTTONS, BUTTON_DEBOUNCE_TIME_MS);
  APP_ERROR_CHECK(err_code);

  app_button_enable();

  err_code = app_timer_create(&m_shutdown_timer, APP_TIMER_MODE_SINGLE_SHOT, shutdown_timer_callback);
  APP_ERROR_CHECK(err_code);

  // Check if gpiote is already initialized (it is, above)
  if (!nrf_drv_gpiote_is_init())
  {
      err_code = nrf_drv_gpiote_init();
      APP_ERROR_CHECK(err_code);
  }

  //err_code = nrfx_gpiote_in_init(chgStatPin, &m_gpiote_input_cfg, gpio_evt_handler);
  //APP_ERROR_CHECK(err_code);

  // Power control setup
  nrf_gpio_cfg_output(PWR_CTRL_PIN);
  pwr_ctrl_on();

  // Extraneous pin setup
  nrf_gpio_cfg_output(7);
  nrf_gpio_pin_write(7, 0);

  nrf_gpio_cfg_output(8);
  nrf_gpio_pin_write(8, 0);

  nrf_gpio_cfg_output(11);
  nrf_gpio_pin_write(11, 0);

  nrf_gpio_cfg_output(13);
  nrf_gpio_pin_write(13, 0);

  nrf_gpio_cfg_output(17);
  nrf_gpio_pin_write(17, 0);

  nrf_gpio_cfg_output(19);
  nrf_gpio_pin_write(19, 0);

  nrf_gpio_cfg_output(22);
  nrf_gpio_pin_write(22, 0);

  nrf_gpio_cfg_output(23);
  nrf_gpio_pin_write(23, 0);

  nrf_gpio_cfg_output(24);
  nrf_gpio_pin_write(24, 0);

  nrf_gpio_cfg_output(26);
  nrf_gpio_pin_write(26, 0);

  nrf_gpio_cfg_output(27);
  nrf_gpio_pin_write(27, 0);
}

void enable_gpio_interrupts()
{
  // nrfx_gpiote_in_event_enable(chgStatPin, true);
}

void button_evt_handler(uint8_t ucPinNum, uint8_t ucButtonAction)
{
  switch(ucPinNum)
  {
    case(BUTTON_PIN):
      if(APP_BUTTON_PUSH == ucButtonAction)
      {
        NRF_LOG_INFO("Button Event: Pressed");
        //set_app_flag(FLAG_BTN_PRESSED);
        app_timer_start(m_shutdown_timer, APP_TIMER_TICKS(GPIO_TIMER_LONG_PRESS_TIME), NULL);
      }
      if(APP_BUTTON_RELEASE == ucButtonAction)
      {
        NRF_LOG_INFO("Button Event: Released");
        //set_app_flag(FLAG_BTN_RELEASE);
        app_timer_stop(m_shutdown_timer);
      }
    break;
    default:
      NRF_LOG_ERROR("Unknown button pressed");
    break;
  }
}

void pwr_ctrl_on(void) // turn on power control
{
  nrf_gpio_pin_set(PWR_CTRL_PIN);
}

void pwr_ctrl_off(void) // turn off power control
{
  nrf_gpio_pin_clear(PWR_CTRL_PIN);
}

bool button_active()
{
  // button is active low
  return(0 == nrf_drv_gpiote_in_is_set(BUTTON_PIN));
}