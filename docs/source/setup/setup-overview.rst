Setup
*****

This section contains information on setting up development environments for compilation of firmware as well as generation of documentation.

.. toctree::
	:maxdepth: 2
	
	setup-compile
	setup-documentation
	setup-unit-testing
	setup-system-testing
	