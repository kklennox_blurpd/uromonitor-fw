/* ========================================
 *
 * Copyright Blur Product Development, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur Product Development.
 *
 * ========================================
*/
/** @file
 *
 * @brief Functions for initializing and handling the LPS22HB pressure sensor.
 */

#ifndef PRESSURE_SENSOR_H__
#define PRESSURE_SENSOR_H__

#include <stdbool.h>
#include <stdint.h>    
#include "FlashInterface.h"

extern bool LPS22HB_init();
static bool LPS22HB_read(uint32_t * pDest);
extern void LPS22HB_GetReading(uint32_t * dest);
extern bool LPS22HB_IsDataReady();

extern void LPS22HB_GetFifo(uint32_t *pucaBuf); 

extern bool LSP22HB_read_fifo();
extern bool LSP22HB_clear_fifo();

#endif // PRESSURE_SENSOR_H__
