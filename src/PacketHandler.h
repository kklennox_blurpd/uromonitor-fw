/* ========================================
 *
 * Copyright BlurPD, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur PD 2022
 *
 * ========================================
*/

#ifndef PACKETHANDLER_H
#define PACKETHANDLER_H

#include "globals.h"
#include "pressure_LPS22HB.h"

void PacketHandlerInit();

void PacketHandlerConstructTimestepPacket();
p_Timestep_t PacketHandlerGetTimestepPacket();

//Power ON, Power OFF, Button Press, I2C Comm Failure
void PacketHandlerConstructLogPacket();
p_Timestep_t PacketHandlerGetLogPacket();

//Calculate Checksum
extern uint8_t PacketHandlerCalculateChecksum(Timestep_t * pInfo);

#endif
/* [] END OF FILE */