###########
Compilation
###########

.. _compilation:

The Uromonitor project utilizes `Segger Embedded Studio <https://www.segger.com/downloads/embedded-studio/>`__. 
Loading the project solution (\*.emProject) using Segger Embedded Studio v5.70a or newer and compiling is sufficient for build. 

************
Auto-Version
************

This project contains an auto-versioning system based on Git. 
It requires Git to be installed and available on the user's path. 

.. warning::
    The default BlurAutoVersion.h file will generate a compilation error
    if git is not installed.


.. _install-chocolatey:

Chocolatey
~~~~~~~~~~

`Chocolatey`__  is recommended for installation of Git, as it is the preferred Installation
method for other tools required for building the complete SmartLink project. 

For detailed instructions, see how to  `install Chocolatey
<https://chocolatey.org/install>`_ from the official documentation.

__ https://chocolatey.org/

.. _install-git:

Git
~~~

.. code-block:: doscon

    $ choco install git.install

As an alternative, `Git for Windows`__ can be installed with a standalone installer.

__ https://gitforwindows.org/

Segger Embedded Studio Integration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To set up auto-version to automatically run upon executing a Build, 
Right-click ``Project -> Options -> Common -> User Build Step`` 
and add ``BlurAutoVersion.bat`` to ``Pre-Compile Command``.

.. image:: assets/build-settings-autoversion.png
  :width: 600
  :alt: Build Settings Menu

***************
Initial Compile
***************

After installing Git, and making sure that the auto-versioning is set to run, it's time to run a test build!

If the build reports success in the Output window, you're all set! 
If you only want to compile, and you won't be changing documentation, 
feel free to skip ahead to :ref:`Unit Testing <setup-unit-testing>`.

Otherwise, let's dig in to setting up our :ref:`Documentation <setup-documentation>`!
