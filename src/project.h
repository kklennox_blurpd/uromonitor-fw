/* ========================================
 *
 * Copyright Blur Product Development, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur Product Development.
 *
 * ========================================
*/

#ifndef PROJECT_H
#define PROJECT_H 
    
/* BOARD SETUP / CUSTOMIZATION */
    // Determines if the system automatically advances through rep tracking/resting,
    // or if user drives rest periods and end of sets via GUI.
    #define USER_DRIVEN_RESTS 


    #define SENSOR_INT_PIN          NRF_GPIO_PIN_MAP(0,20)  /**< Pressure sensor interrupt, configured to pulse on data ready */
    #define BUTTON_PIN              NRF_GPIO_PIN_MAP(0,4)   /**< Digital input - low when button is pressed */ 
    #define PWR_CTRL_PIN            NRF_GPIO_PIN_MAP(0,14)  /**< GPIO Pin that will latch power on, or allow device to shut itself off. */    

    // SPI Chip Select setup
    //#define FLASH_CS_ON                     DigOut_SPI_CS_0_Write(LOW);CyDelayUs(15)
    //#define FLASH_CS_OFF                    CyDelayUs(15);DigOut_SPI_CS_0_Write(HI);CyDelayUs(15)
    
/* END BOARD SETUP / CUSTOMIZATION */

#define CLI_RTT_TERMINAL_NUM  (0) /// Terminal to use for CLI integration
    
#define LOW             (0x00u)
#define HI              (0x01u)

#define SWAP_DWORD(num) ((num & 0xff000000) >> 24) | ((num & 0x00ff0000) >> 8) | ((num & 0x0000ff00) << 8) | (num << 24)
#define SWAP_BYTES(num) ((num & 0xff00) >> 8) | ((num & 0x00ff) << 8)
#define MAKE_WORD(a,b)  (uint16)(((uint16)(a) << 8) + b)
    
#define LO_BYTE(a)  (uint8)(0x00FFu & (uint16)a)
#define HI_BYTE(a)  (uint8)(0x00FFu & (uint16)((uint16)a >> 8))

//extern void ErrorHandler(enum eSystemErrorCodes eErr);

// to be implemented
#if(0)
extern void kickWatchdog();
extern void determineSystemErrorCode();
#endif

#endif

/* [] END OF FILE */
