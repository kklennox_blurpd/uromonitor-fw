# Bright Uro UroMonitor Firmware 

UroMonitor firmware based on the Nordic nRF52811 BLE chipset

Build project with Segger Embedded Studio v6.20a or above.

/sdk contains the Nordic nRF5 SDK
/src contains application source code

## Initial Setup

Make sure Git is installed and available on the cmd PATH:
```
> git status 
```

Install necessary packages:
Tools -> Package Manager -> CMSIS DSP Support Package install (it will automatically install core package as well).

Support Package versions:
CMSIS-CORE
CMSIS-DSP

To compile, Open Solution in /src

## Auto Versioning

This project integrates the Blur Auto-Versioning tool. To use, add annotated tags to desired commits with the format ```x.y.z```. 
This follows the symver naming convention, of major.minor.build numbering and encodes backwards and forwards compatibility.
This information is available to the firmware, see BlurAutoVersion.h. 
It is exposed to outside applications via BLE Device Information Service (DIS), as well as the CLI command 'id'.
Additionally, it exposes information on whether the build is based on a clean (ie committed with no changes) repository, or one in which there are changes that have not been committed.

#### Example 

```
id
> ID,nRF52811,0.0.2.1-g655f888+
```

```0.0.2``` corresponds to major.minor.build numbering scheme. ```.1``` corresponds to how many commits behind the last tagged commit is, and ```-g655f888+``` corresponds to the most recent git commit hash, with ```+``` indicating that the build is dirty and based on code that is not committed.


## Powering the device

The current dev board requires 1.9 (MAX 2.0V) provided to the VDD line. This line is on J10, NOT through the banana plug. 

## Launching CLI

This project contains a command line interface through Segger RTT (Real-time transfer).

To connect:

- Attach Segger J-Link to programming port
- Provide power to the device
- Launch J-Link RTT Viewer
- Connect through File->Connect
- Change send-type to Input->Sending...->Send on Enter

## Errata

After programming, the device hangs and requires a power cycle of VDD. This will be evidenced by the RTT Viewer displaying 'Fatal Error'. 
After power cycling, the RTT Viewer will need to be reconnected through File->Connect

## Notes

Modification to file paths may be necessary for ARM_MATH. 
Additional details here:
https://devzone.nordicsemi.com/f/nordic-q-a/27715/nrf52832-how-to-include-the-arm_math-h-library

