Power Profiling
***************

The `Power Profiler Kit v2 (PPK)`__ from Nordic Semiconductor is a tool 
to measure power consumption of wearable devices in real time, 
by providing power to the wearable in place of the battery (or in series with the battery). 

Although the system **does not have a calibration procedure**, 
it can still be used as a diagnostic tool to verify rough power 
consumption parameters to indicate some board failures. 

**For a calibrated tool**, the `Joulescope`__ is also available 
and provides enhanced functionality. 


 __ https://www.nordicsemi.com/Software-and-tools/Development-Tools/Power-Profiler-Kit-2
 __ https://www.joulescope.com/
 
 
Setup
#####

1. Install the `NRF Connect`__ application, and then the Power Profiler sub-application.
2. Plug a USB cable into the USB DATA/POWER receptacle.
3. Plug the USB cable into the computer
4. Plug the battery power and battery ground leads from the test jig into the PPK. See  `here`__ for a description. 
5. Start the nRF Connect app, and launch the Power Profiler app. 
6. Select the PPK on the left side, and connect to the device.
7. Change the output voltage to 4.0V
8. Turn off the output power via the slider.

 __ https://www.nordicsemi.com/Software-and-tools/Development-Tools/nRF-Connect-for-desktop/Download#infotabs
 
 __ https://infocenter.nordicsemi.com/index.jsp?topic=%2Fug_ppk2%2FUG%2Fppk%2FPPK_user_guide_Quick_start.html