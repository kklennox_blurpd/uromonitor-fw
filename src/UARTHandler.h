/* ========================================
 *
 * Copyright BlurPD, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur PD 2022
 *
 * ========================================
*/

#ifndef UARTHANDLER_H
#define UARTHANDLER_H

#define UART_HWFC APP_UART_FLOW_CONTROL_DISABLED

#include <stdbool.h>
#include "nrf_drv_uart.h"
#include <stdint.h>
#include <stdio.h>
#include "app_uart.h"
#include "app_error.h"
#include "nrf_delay.h"
#include "nrf.h"
#include "nrf_drv_gpiote.h"

//#include "CommandHandler.h"
//#include "bsp.h"
#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined (UARTE_PRESENT)
#include "nrf_uarte.h"
#endif

#define UART_ENABLE_PIN                 27
#define UART_TEST_MODE                  0

#define UART_BUFFER_SIZE  1024
    
//Initialize the UART interface. 
void UARTHandlerInit();
//Read any data present in the FIFO
void UARTHandlerProcessEvents();
//Reset the buffers for next incoming packet
void UARTHanlderReset();
//Write the serial buffer out the serial port
void UARTHandlerWriteBuffer(uint16_t uiCount);

//Return a pointer to the serial buffer
uint8_t* UARTHandlerGetSerialBuffer();
//Return the number of bytes in the serial buffer
uint16_t UARTHandlerGetInputByteCount();

void UARTHandlerClose();

#endif
/* [] END OF FILE */
