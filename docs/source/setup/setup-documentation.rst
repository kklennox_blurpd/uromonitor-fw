#############
Documentation
#############

.. _setup-documentation:

This documentation is generated with `Sphinx`__. 
Sphinx allows for generation of intelligent and organized documentation in plain-text, 
that can also reside in revision control alongside the firmware it references. 

Firmware related documentation is generated with `Doxygen`__. 
Doxygen generates documentation directly from comments in source code.

`Breath`__ acts as the bridge between Doxygen and Sphinx. 
It translates Doxygen formatted documents into a Sphinx compatible format. 

.. graphviz::
    :name: sphinx.ext.graphviz
    :caption: Sphinx and GraphViz Data Flow
    :alt: How Sphinx and GraphViz Render the Final Document
    :align: center

     digraph "sphinx-ext-graphviz" {
         size="6,4";
         rankdir="LR";
         graph [fontname="Verdana", fontsize="12"];
         node [fontname="Verdana", fontsize="12"];
         edge [fontname="Sans", fontsize="9"];

         sphinx [label="Sphinx", shape="component",
                   href="https://www.sphinx-doc.org/",
                   target="_blank"];
         dot [label="GraphViz", shape="component",
              href="https://www.graphviz.org/",
              target="_blank"];
         docs [label="Docs (.rst)", shape="folder",
               fillcolor=green, style=filled];
         svg_file [label="SVG Image", shape="note", fontcolor=white,
                 fillcolor="#3333ff", style=filled];
         html_files [label="HTML Files", shape="folder",
              fillcolor=yellow, style=filled];

         docs -> sphinx [label=" parse "];
         sphinx -> dot [label=" call ", style=dashed, arrowhead=none];
         dot -> svg_file [label=" draw "];
         sphinx -> html_files [label=" render "];
         svg_file -> html_files [style=dashed];
     }

The following setup is only required if a user or developer wishes to compile the documentation from source, 
if changes are made.

 __ https://www.sphinx-doc.org/en/master/
 __ https://www.doxygen.nl/index.html
 __ https://breathe.readthedocs.io/
 
 
Installing
============

To install, open a Windows command prompt or Powershell terminal in Administrator mode.
Run the following commands:

Chocolatey
~~~~~~~~~~

Chocolately is recommended for installation of required compilation tools.
See the 
:ref:`install guide <install-chocolatey>` in the Compilation section.

Sphinx
~~~~~~

.. code-block:: doscon

   $ choco install sphinx

Doxygen
~~~~~~~
.. code-block:: doscon

   $ choco install doxygen.install

Breathe
~~~~~~~

.. code-block:: doscon

   $ pip install breathe

Graphviz
~~~~~~~~

.. code-block:: doscon

   $ choco install graphviz

PATH Config
~~~~~~~~~~~

.. code-block:: doscon

   $ $env:path="$env:Path;C:\Python310;C:\Python310\scripts"

Theme
~~~~~

.. code-block:: doscon

   $ pip install sphinx-rtd-theme
 
Build Local
===========
Using Powershell or cmd, navigate to the base level docs/ directory.

.. code-block:: doscon

   $ sphinx-build.exe -b html source build -a

Build Remote
============
Gitlab allows for hosting static pages (known as Pages). 
Pages must be enabled in the administrator settings for a given repository. 
The `Gitlab Sphinx <https://gitlab.com/pages/sphinx>`_ repository provides a good sample to base off of.
These pages are built remotely, and require the following additions to ``.gitlab-ci.yml``.

.. code-block:: python

   image: python:3.7-alpine

   test:
   stage: test
   script:
   - pip install -U sphinx
   - sphinx-build -b html . public
   only:
   - branches
   except:
   - master

   pages:
   stage: deploy
   script:
   - pip install -U sphinx
   - sphinx-build -b html . public
   artifacts:
      paths:
      - public
   only:
   - master

Release Notes
=============

.. code-block:: doscon

   $ git log --pretty=format:"%h %as %<(13) %d %s" > release-notes.txt

Reference Guide
===============

The official Sphinx 
`Reference Guide <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_ 
provides helpful documentation on writing with reStructured Text. 
For a more compact reference guide, try the `rest-sphinx-memo <https://rest-sphinx-memo.readthedocs.io/en/latest/ReST.html>`__ repo. 
As an alternative, see the handy guide from `Quick reStructuredText <https://docutils.sourceforge.io/docs/user/rst/quickref.html>`__.