/* ========================================
 *
 * Copyright BlurPD, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur PD 2022
 *
 * ========================================
*/

#include "SPIHandler.h"

#define SPI_INSTANCE    0

static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */


void SPIHandlerInit()
{
    nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
    spi_config.mode = NRF_DRV_SPI_MODE_0;
    spi_config.ss_pin   = NRF_DRV_SPI_PIN_NOT_USED;
    spi_config.miso_pin = SPI_MISO_PIN;
    spi_config.mosi_pin = SPI_MOSI_PIN;
    spi_config.sck_pin  = SPI_CLK_PIN;
    APP_ERROR_CHECK(nrf_drv_spi_init(&spi, &spi_config, NULL, NULL));

    NRF_LOG_INFO("SPI Driver Started.");
}

void SPIWriteByte(uint8_t byte)
{ 
    nrf_drv_spi_transfer(&spi, &byte, 1, NULL, 0);
}

uint8_t SPIReadByte()
{ 
    ret_code_t result;
    uint8_t byte;

    result = nrf_drv_spi_transfer(&spi, NULL, 0, &byte, 1);

    return(byte);
}

void SPIReadBuffer(uint8_t *pucaData,uint16_t uiCount)
{
    for(int uiSPILoop = 0;uiSPILoop < uiCount;uiSPILoop++)
    {
        *pucaData = SPIReadByte();
        pucaData++;
    }
}

void SPIReadBufferFast(uint8_t *pucaData,uint8_t uiCount)
{
    nrf_drv_spi_transfer(&spi, NULL, 0, pucaData, uiCount);
}

/* [] END OF FILE */

