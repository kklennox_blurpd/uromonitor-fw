#########################
Bulk Data Characteristic
#########################

.. |packet-size| replace:: 130

.. contents::

.. _uromonitor-characteristic-bulk-data:

The Bulk Data characteristic provides the central device with 
the ability to rapidly download the data stored in flash memory on the Uromonitor.

.. list-table:: 
   :widths: 50 25 25
   :header-rows: 1

   * - UUID
     - Properties
     - Permissions
   * - ``88ED0002-722D-0055-02DF-A2340946D9C0``
     - Notify
     - Encrypted

Bulk data contains the raw data collected during a timestep and includes |packet-size| bytes of data for each timestep. 
The :ref:`Bulk Data command <uromonitor-characteristic-opcommand-bulk-data>` in the :ref:`OpCommand Characteristic <uromonitor-characteristic-opcommand>`  starts the streaming process.

***********
Data Format
***********

The bulk data characteristic is represented by a struct in the firmware:

.. list-table:: 
   :widths: 25 25 25 50
   :header-rows: 1

   * - Byte Position
     - Format
     - Parameter
     - Description
   * - 1
     - uint32
     - Timestep Index
     - Index of packet in flash memory
   * - 5
     - uint32
     - Timestep Count
     - Total number of timesteps present in flash memory
   * - 9
     - uint8[|packet-size|]
     - :ref:`Timestep <packet-format>`
     - Timestep data, see :ref:`data format <packet-format>`
   * - 9 + |packet-size|
     - uint16
     - E2E-Checksum
     - 16 bit checksum calculated over the entire packet