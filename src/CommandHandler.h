/* ========================================
 *
 * Copyright BlurPD, 2022
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Blur PD 2022
 *
 * ========================================
*/

#ifndef COMMANDHANDLER_H
#define COMMANDHANDLER_H

#include "globals.h"
#include "BlurPacketProtocol.h"
#include "pressure_LPS22HB.h"
#include "UARTHandler.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "FlashInterface.h"

//Initialize the command handler interface. This must be called before any other functions herein
void commandHandlerInit();
//See if the input serial buffer contains a complete incoming command
bool commandHandlerHasCompleteIncomingCommand();
//Process the input serial command packet
void commandHandlerProcessIncomingCommand();
//Get the number of bytes in the response packet back to the host
uint16_t commandHandlerGetResponseByteCount();


#endif
/* [] END OF FILE */